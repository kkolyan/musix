package com.nplekhanov.musix;

import com.nplekhanov.musix.model.Track;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class TrackUpdate<T> {
    private final String descriptionPattern;
    private final Function<Track, T> get;
    private final BiConsumer<Track, T> set;
    private final T value;

    public TrackUpdate(String descriptionPattern, Function<Track, T> get, BiConsumer<Track, T> set, T value) {
        this.descriptionPattern = descriptionPattern;
        this.get = get;
        this.set = set;
        this.value = value;
    }

    public String getDescriptionPattern() {
        return descriptionPattern;
    }

    public Function<Track, T> getGet() {
        return get;
    }

    public BiConsumer<Track, T> getSet() {
        return set;
    }

    public T getValue() {
        return value;
    }
}
