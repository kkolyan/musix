<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="com.nplekhanov.musix.model.Opinion" %>
<%@ page import="com.nplekhanov.musix.model.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="java.util.Objects" %>
<%@ page import="com.nplekhanov.musix.model.Attitude" %>
<%@ page import="java.util.NavigableSet" %>
<%@ page import="java.util.TreeSet" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Персональный чарт</title>
    <jsp:include page="mobile.jsp"/>
    <link rel="stylesheet" href="common.css" type="text/css"/>
    <script>
        var lastSize = 0;

        function resizeIframe(obj) {
            var desiredSize = obj.contentWindow.document.body.scrollHeight;
            if (lastSize != desiredSize) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
                lastSize = desiredSize;
            }
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="scroll.js" type="text/javascript"></script>
</head>
<body>
<jsp:include page="header.jsp"/>

Оценки треков используются для определения приоритета между треками с одинаковым кол-вом желающих.
Абсолютные значения оценок не важны - учитываются только сравнительные отношения между треками каждого пользователя.
<div><%
    String currentUserId = (String) session.getAttribute("userId");
    String userId = request.getParameter("userId");
    Musix musix = (Musix) application.getAttribute("musix");
    String filter = request.getParameter("filter");
    if (filter != null && filter.trim().isEmpty()) {
        filter = null;
    }
    if (userId == null) {
        userId = currentUserId;
    }
    NavigableSet<Integer> steps = new TreeSet<>();
    steps.addAll(Arrays.asList(1));
    List<Map.Entry<String, Opinion>> orderedTracks = musix.getOrderedTracks(userId);
    StringBuffer source = request.getRequestURL();
    if (request.getQueryString() != null) {
        source.append("?").append(request.getQueryString());
    }
    for (User user: musix.getDefaultBand().values()) {
%>
    <a href="personal_chart.jsp?userId=<%=escapeHtml4(user.getUid())%>">
        <img width="64" src="<%=user.getPhotoUrl()%>" title="<%=escapeHtml4(user.getFullName())%>" alt="<%=escapeHtml4(user.getFullName())%>"/>
    </a>
    <%
        }
    %></div>
<form>
    <label>
        Фильтр
        <input name="filter" value="<%=filter == null ? "" : escapeHtml4(filter)%>"/>
    </label>
    <input type="submit" value="Отфильровать"/>
</form>
<% if (filter != null) {
    %>Показаны только треки, включающие буквосочетание <b>&quot;<%=escapeHtml4(filter)%>&quot;</b> <%
} %>

<div>
    <form action="PersonalTrackRating" style="display: inline;" method="post" onsubmit="return updateScrollState(this)">
        <input type="hidden" name="onlyDesired" value="true"/>
        <input type="hidden" name="step" value="-1"/>
        <input type="submit" value="-1 to desired"/>
        <input type="hidden" name="source" value="<%=source%>">
    </form>
    <form action="PersonalTrackRating" style="display: inline;" method="post" onsubmit="return updateScrollState(this)">
        <input type="hidden" name="onlyDesired" value="true"/>
        <input type="hidden" name="step" value="1"/>
        <input type="submit" value="+1 to desired"/>
        <input type="hidden" name="source" value="<%=source%>">
        <input type="hidden" name="scrollState" value="scrollState1"/>
    </form>
</div>
<div>
    <form action="PersonalTrackRating" style="display: inline;" method="post" onsubmit="return updateScrollState(this)">
        <input type="hidden" name="allTracks" value="true"/>
        <input type="hidden" name="step" value="-1"/>
        <input type="submit" value="-1 to all"/>
        <input type="hidden" name="source" value="<%=source%>">
        <input type="hidden" name="scrollState" value="scrollState1"/>
    </form>
    <form action="PersonalTrackRating" style="display: inline;" method="post" onsubmit="return updateScrollState(this)">
        <input type="hidden" name="allTracks" value="true"/>
        <input type="hidden" name="step" value="1"/>
        <input type="submit" value="+1 to all"/>
        <input type="hidden" name="source" value="<%=source%>">
        <input type="hidden" name="scrollState" value="scrollState1"/>
    </form>
</div>
<%

    long lastRating = Long.MAX_VALUE;
    for (Map.Entry<String, Opinion> track: orderedTracks) {
        if (filter != null && !track.toString().toLowerCase().contains(filter.toLowerCase())) {
            continue;
        }

        if (lastRating != track.getValue().getRating()) {
%><br/> <%
        }
%> <div  <% if (track.getKey().equals(request.getParameter("highlight"))) {%> style="font-weight: bold;" <%}%> ><%
    if (track.getValue().getAttitude() != Attitude.UNACCEPTABLE) {
%>


    <%
        if (Objects.equals(currentUserId, userId)) {
            for (Integer step: steps.descendingSet()) {
    %>
    <form action="PersonalTrackRating" style="display: inline;" method="post" onsubmit="return updateScrollState(this)">
        <input type="hidden" name="filter" value="<%=filter == null ? "" : escapeHtml4(filter)%>"/>
        <input type="hidden" name="track" value="<%=escapeHtml4(track.getKey())%>"/>
        <input type="hidden" name="step" value="-<%=step%>"/>
        <input type="submit" value="-<%=step%>"/>
        <input type="hidden" name="source" value="<%=source%>">
        <input type="hidden" name="scrollState" value="scrollState1"/>
    </form>
    &nbsp;
    <%
            }
        }
        for (int i = 0; i <= 5; i++) {
            if (i == track.getValue().getRating()) {
            } else {
            }
            %> <form action="PersonalTrackRating" style="display: inline;" method="post" onsubmit="return updateScrollState(this)">
        <input type="hidden" name="filter" value="<%=filter == null ? "" : escapeHtml4(filter)%>"/>
        <input type="hidden" name="track" value="<%=escapeHtml4(track.getKey())%>"/>
        <input type="hidden" name="value" value="<%=i%>"/>
        <% if (i == track.getValue().getRating()) {%>
        <input type="submit" style="margin: -3px; background-color: white; border-style: solid" value="<%=i%>"/>
        <%} else {%>
        <input type="submit" style="margin: -3px;" value="<%=i%>"/>
        <%}%>
        <input type="hidden" name="source" value="<%=source%>">
        <input type="hidden" name="scrollState" value="scrollState1"/>
    </form> <%
        }
        if (Objects.equals(currentUserId, userId)) {
            for (Integer step: steps) {
    %>
    &nbsp;
    <form action="PersonalTrackRating" style="display: inline;" method="post" onsubmit="return updateScrollState(this)">
        <input type="hidden" name="filter" value="<%=filter == null ? "" : escapeHtml4(filter)%>"/>
        <input type="hidden" name="track" value="<%=escapeHtml4(track.getKey())%>"/>
        <input type="hidden" name="step" value="<%=step%>"/>
        <input type="submit" value="+<%=step%>"/>
        <input type="hidden" name="source" value="<%=source%>">
        <input type="hidden" name="scrollState" value="scrollState1"/>
    </form>
    <%
                }
            }
        }
    %>

    <a target="_parent" name="<%=escapeHtml4(track.getKey())%>" class="<%=track.getValue().getAttitude()%>" href="rate.jsp?track=<%=URLEncoder.encode(track.getKey(), "utf-8")%>">
        <%=escapeHtml4(musix.getTrackInfo().get(track.getKey()).getTrackName())%>
    </a>

</div>
<%
        lastRating = track.getValue().getRating();
    }
%>

</body>
</html>
