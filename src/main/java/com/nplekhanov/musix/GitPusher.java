package com.nplekhanov.musix;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.BranchConfig;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class GitPusher {
    private static final Logger logger = LoggerFactory.getLogger(GitPusher.class);

    public static String getGitInfo(File dir) throws IOException {
        StringBuilder s = new StringBuilder();
        try (Git repo = Git.open(dir)) {
            for (String s1 : repo.status().call().getModified()) {
                s.append("modified: ").append(s1).append("\n");
            }
            Map<ObjectId, List<Ref>> branches = repo.branchList()
                .setListMode(ListBranchCommand.ListMode.ALL)
                .call().stream()
                .collect(Collectors.groupingBy(Ref::getObjectId));
            Iterable<RevCommit> log = repo.log()
                .call();

            for (RevCommit commit : log) {
                PersonIdent ident = commit.getAuthorIdent();
                s.append(ident.getWhen().toInstant().toString().replace("T", " "));
                s.append(" ").append(commit.abbreviate(7).name());
                List<Ref> branch = branches.get(commit.getId());

                if (branch != null) {
                    for (Ref ref : branch) {
                        s.append(" [");
                        s.append(ref.getName());
                        s.append("]");
                    }
                }
                s.append("\n");
            }

        } catch (GitAPIException e) {
            throw new RuntimeException(e);
        }
        return s.toString();
    }

    public static class DoOnce {
        public static void main(String[] args) throws GitAPIException, IOException {
            commitAndPushChanges(new File("musix_batumi"));
        }
    }

    public static void commitAndPushChanges(File dir) throws GitAPIException, IOException {
        try (Git repo = Git.open(dir)) {
            Status status = repo.status()
                .call();
            if (!status.getModified().isEmpty()) {
                logger.info("doing backup '.git' subdirectory of {} to be able to recover from corruption (blackouts case)", dir);
                protectFromBlackOut(dir);
                logger.info("committing changes: {}", status);
                RevCommit commit = repo.commit()
                    .setAll(true)
                    .setMessage(GitPusher.class.getSimpleName())
                    .call();
                logger.info("committed: {}", commit);
            }

            Map<String, ObjectId> branches = repo.branchList()
                .setListMode(ListBranchCommand.ListMode.ALL)
                .call().stream()
                .collect(Collectors.toMap(Ref::getName, Ref::getObjectId));

            ObjectId head = branches.get(repo.getRepository().getFullBranch());
            ObjectId remoteHead = branches.get(new BranchConfig(repo.getRepository().getConfig(), repo.getRepository().getBranch()).getTrackingBranch());
            if (!Objects.equals(head, remoteHead)){
                logger.info("pushing...");
                Iterable<PushResult> pushResults = repo.push()
                    .setCredentialsProvider(new UsernamePasswordCredentialsProvider("token", ConfigFactory.getConfig().getGithubPat()))
                    .call();
                for (PushResult pushResult : pushResults) {
                    logger.info("pushed: {}", pushResult);
                }
            }
        }
    }

    private static void protectFromBlackOut(File dir) throws IOException {
        File backupDir = new File(dir,".git.backup");
        FileUtils.deleteDirectory(backupDir);
        FileUtils.copyDirectory(new File(dir, ".git"), backupDir);
    }
}
