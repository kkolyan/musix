<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="com.nplekhanov.musix.model.Opinion" %>
<%@ page import="com.nplekhanov.musix.model.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="java.util.Objects" %>
<%@ page import="com.nplekhanov.musix.model.Attitude" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.NavigableSet" %>
<%@ page import="java.util.TreeSet" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <link rel="stylesheet" href="common.css" type="text/css"/>
</head>
<%--<html>--%>
<%--<head>--%>
    <%--<title>Персональный чарт</title>--%>
    <%--<jsp:include page="mobile.jsp"/>--%>
    <%--<link rel="stylesheet" href="common.css" type="text/css"/>--%>
<%--</head>--%>
<%--<body>--%>
<%--<jsp:include page="header.jsp"/>--%>
    <%
        Musix musix = (Musix) application.getAttribute("musix");
        String currentUserId = (String) session.getAttribute("userId");
        String userId = request.getParameter("userId");
        String filter = request.getParameter("filter");
        if (userId == null) {
            userId = currentUserId;
        }
        List<Map.Entry<String, Opinion>> orderedTracks = musix.getOrderedTracks(userId);

        NavigableSet<Integer> steps = new TreeSet<>();
        steps.addAll(Arrays.asList(1));

        %>

<%--</body>--%>
<%--</html>--%>
