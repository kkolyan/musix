package com.nplekhanov.musix.model;

public class TrackRatingChangedEvent extends UserTrackEvent {
    private long rating;

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }
}
