package com.nplekhanov.musix.model;

/**
 * Created by nplekhanov on 07/12/2017.
 * @deprecated
 */
@Deprecated
public class TrackRatedEvent extends UserTrackEvent {
    private Opinion opinion;

    public Opinion getOpinion() {
        return opinion;
    }

    public void setOpinion(Opinion opinion) {
        this.opinion = opinion;
    }
}
