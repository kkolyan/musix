<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="com.nplekhanov.musix.model.User" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="com.nplekhanov.musix.Config" %>
<%@ page import="com.nplekhanov.musix.ConfigFactory" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
Config cfg = ConfigFactory.getConfig();
String vkAppId = cfg.getVkAppId();
Musix musix = (Musix) application.getAttribute("musix");
%>
<html>
<head>
    <jsp:include page="mobile.jsp"/>
    <title>Вход в систему</title>
</head>
<body>

<%
if (request.getServerName().equals("localhost")) {
    %>
    <form action="Login">
        <label>
            uid
            <input name="uid"/>
        </label>
        <label>
            first_name
            <input name="first_name"/>
        </label>
        <label>
            last_name
            <input name="last_name"/>
        </label>
        <input type="hidden" name="hash" value=""/>
        <input type="submit" value="Войти"/>
    </form>
    <%
    for (User user: musix.getDefaultBand().values()) {
        %> <a href="Login?uid=<%=user.getUid()%>"> <img width="128" src="<%=user.getPhotoUrl()%>" title="<%=escapeHtml4(user.getFullName())%>"/> </a> <%
    }
    %>
    <%
} else {
    %>
    <!-- Put this script tag to the <head> of your page -->
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?139"></script>

    <script type="text/javascript">
        VK.init({apiId: <%= escapeHtml4(vkAppId) %>});
    </script>
    <!-- Put this div tag to the place, where Auth block will be -->
    <div id="vk_auth"></div>
    <script type="text/javascript">
        VK.Widgets.Auth("vk_auth", {width: "200px", authUrl: '${pageContext.request.contextPath}/Login'});
    </script>

    <div>
        <a href="neogauth.jsp">Google SSO</a>
    </div>
    <%
}
%>
</body>
</html>
