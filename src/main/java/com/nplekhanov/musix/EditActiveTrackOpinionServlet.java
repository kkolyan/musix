package com.nplekhanov.musix;

import com.nplekhanov.musix.model.User;
import com.nplekhanov.musix.model.active.TrackGradeAspect;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by nplekhanov on 26/12/2017.
 */
public class EditActiveTrackOpinionServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Musix musix = (Musix) req.getServletContext().getAttribute("musix");


        String userId = (String) req.getSession().getAttribute("userId");

        User currentUser = musix.getUser(userId);

        String track = req.getParameter("track");
        String comment = req.getParameter("comment");
        Integer newPriority = null;
        if (currentUser.hasAdminRights()) {
            newPriority = Integer.valueOf(req.getParameter("priority"));
        }
        Map<TrackGradeAspect, Double> grades = new TreeMap<>();
        for (TrackGradeAspect aspect : TrackGradeAspect.values()) {
            String gradeValue = req.getParameter(aspect.name());
            if (!gradeValue.isEmpty()) {
                grades.put(aspect, Double.valueOf(gradeValue));
            }
        }
        musix.submitActiveTrackOpinion(userId, track, newPriority, comment, grades);
        resp.sendRedirect("active.jsp");
    }
}
