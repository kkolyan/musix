package com.nplekhanov.musix.model;

public class TrackCommentChangedEvent extends UserTrackEvent {
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
