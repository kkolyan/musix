package com.nplekhanov.musix.model;

public class TrackAttitudeChangedEvent extends UserTrackEvent {
    private Attitude attitude;

    public Attitude getAttitude() {
        return attitude;
    }

    public void setAttitude(Attitude attitude) {
        this.attitude = attitude;
    }
}
