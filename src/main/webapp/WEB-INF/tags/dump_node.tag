<%@ tag pageEncoding="utf-8" %>
<%@ tag import="java.util.Iterator" %>
<%@ taglib prefix="mx" tagdir="/WEB-INF/tags" %>
<%@ attribute name="value" type="com.fasterxml.jackson.databind.JsonNode" required="true" %>
<%@ attribute name="path" type="java.util.List" required="false" %>
<%@ tag import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ tag import="com.fasterxml.jackson.databind.JsonNode" %>
<%@ tag import="java.util.List" %>
<%@ tag import="java.util.ArrayList" %>
<%@ tag import="com.nplekhanov.musix.JsonRepository" %>
<%@ tag import="java.net.URLEncoder" %>
<%@ tag import="java.io.UnsupportedEncodingException" %>

<%!
private List<?> addPath(List<?> currentPath, Object element) {
    List<Object> list;
    if (currentPath == null) {
        list = new ArrayList<>();
    } else {
        list = new ArrayList<>(currentPath);
    }
    list.add(element);
    return list;
}

private String preview(JsonNode node) {
    String s = node.toString();
    int maxLength = 100;
    if (s.length() > maxLength) {
        return s.substring(0, maxLength - 3)+"...";
    }
    return s;
}

private String urlEncode(String s) throws UnsupportedEncodingException {
    return URLEncoder.encode(s, "UTF-8");
}
%>
<%

    JsonRepository repository = (JsonRepository) application.getAttribute("repository");
%>

<div class="dump node" title="<%=escapeHtml4(urlEncode(repository.encodePath(path)))%>">
    <%--<a href="#">открыть</a> <a href="#">редактировать</a>--%>
    <%
    if (value.isArray()) {
        %>
        <p class="bracket">[ </p>
        <%
            for (int i = 0; i < value.size(); i++) {
                String childElement = "["+i+"]";
                %>
                <div>
                    <span class="field_title">
                        <span class="field_name"><%=escapeHtml4(childElement)%></span>
                        <span class="preview"><%=escapeHtml4(preview(value.get(i)))%></span>
                        <a href="dump.jsp?path=<%=escapeHtml4(urlEncode(repository.encodePath(addPath(path, i))))%>">ред.</a>
                        <a href="dump_delete_node.jsp?path=<%=escapeHtml4(urlEncode(repository.encodePath(addPath(path, i))))%>">удал.</a>
                    </span>
                </div>
                <div class="toggleable">
                    <mx:dump_node value="<%=value.get(i)%>" path="<%=addPath(path, i)%>"/>
                </div>
                <%
            }
            %> <a href="dump.jsp?path=<%=escapeHtml4(urlEncode(repository.encodePath(addPath(path, value.size()))))%>">[ + ]</a> <%
        %>
        <p class="bracket">]</p>
        <%
    } else if (value.isObject()) {
        %>
        <p class="bracket">{ </p>
        <%
            for (Iterator<String> it = value.fieldNames(); it.hasNext();) {
                String fieldName = it.next();
                %>
                <div>
                    <span class="field_title">
                        <span class="field_name"><%=escapeHtml4(fieldName)%></span>
                        <span class="preview"><%=escapeHtml4(preview(value.get(fieldName)))%></span>
                        <a href="dump.jsp?path=<%=escapeHtml4(urlEncode(repository.encodePath(addPath(path, fieldName))))%>">ред.</a>
                    </span>
                </div>
                <div class="toggleable" style="display: none">
                    <mx:dump_node value="<%=value.get(fieldName)%>" path="<%=addPath(path, fieldName)%>"/>
                </div>
                <%
            }
        %>
        <p class="bracket">}</p>
        <%
    } else if (value.isNumber()) {
        %> <span class="number"><%=escapeHtml4(value.toString())%></span> <%
    } else if (value.isBoolean()) {
        %> <span class="boolean"><%=escapeHtml4(value.toString())%></span> <%
    } else if (value.isNull()) {
        %> <span class="null">null</span> <%
    } else if (value.isTextual()) {
        %> <span class="text">&quot;<%=escapeHtml4(value.asText())%>&quot;</span> <%
    } else {
        throw new IllegalStateException(""+value);
    }
    %>
</div>