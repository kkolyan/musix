package com.nplekhanov.musix;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by nplekhanov on 03/10/2017.
 */
public class ConfigFactory {

    private static Properties loadConfigProperties() throws IOException {
        Properties props = new Properties();
        try (FileInputStream stream = new FileInputStream(getConfigFile())){
            props.load(stream);
        }
        return props;
    }

    private static File getConfigFile() {
        return new File(System.getProperty("musix.config.file", "/home/musix/musix.cfg"));
    }

    public static Config getConfig() {
        if (!getConfigFile().exists()) {
            throw new IllegalStateException("can't find "+ getConfigFile().getAbsolutePath());
        }

        Properties props;
        try {
            props = loadConfigProperties();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return new Config() {
            @Override
            public String getVkAppId() {
                return props.getProperty("vk.appId");
            }

            @Override
            public String getGoogleClientId() {
                return props.getProperty("google.clientId");
            }

            @Override
            public String getSecretKey() {
                return props.getProperty("vk.secretKey");
            }

            @Override
            public String getDevIp() {
                return props.getProperty("devIp");
            }

            @Override
            public String getBackupSuffixDateTimeFormat() {
                return props.getProperty("backup.suffix.dateTimeFormat");
            }

            @Override
            public String getTracksExternalListeningPage() {
                return props.getProperty("opinions.tracksListeningPage");
            }

            @Override
            public String getGithubPat() {
                return props.getProperty("github.pat");
            }

            @Override
            public String getRepositoryFilePath() {
                return props.getProperty("repository.file.path");
            }
        };
    }
}
