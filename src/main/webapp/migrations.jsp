<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="com.nplekhanov.musix.model.User" %>
<%@ page import="com.nplekhanov.musix.JsonRepository" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.io.StringWriter" %>
<%@ page import="com.nplekhanov.musix.migrations.MoveOpinionsFromUserToTracks" %>
<%
    JsonRepository repository = (JsonRepository) application.getAttribute("repository");
    StringWriter buffer = new StringWriter();
    PrintWriter writer = new PrintWriter(buffer, true);
    try {
        new MoveOpinionsFromUserToTracks().apply(repository);
    } catch (Exception e) {
        e.printStackTrace(writer);
    }
%>
<html>
<body>
<jsp:include page="header.jsp"/>
<pre><%=buffer%></pre>
</body>
</html>