package com.nplekhanov.musix;

import com.nplekhanov.musix.model.Database;

/**
 * Created by nplekhanov on 5/14/2017.
 */
public interface Repository {

    void change(UsersChange callback);

    <T> T read(UsersRead<T> callback);

    interface UsersChange {
        boolean tryChange(Database db);
    }

    interface UsersRead<T> {
        T read(Database db);
    }

    default void close() {}
}
