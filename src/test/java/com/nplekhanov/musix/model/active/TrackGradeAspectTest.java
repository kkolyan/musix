package com.nplekhanov.musix.model.active;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by nplekhanov on 27/12/2017.
 */
public class TrackGradeAspectTest {

    @Test
    public void test01() {
        test(0, 0);
    }

    @Test
    public void test07() {
        test(0.5, 0);
    }

    @Test
    public void test08() {
        test(0.6, 20);
    }

    @Test
    public void test09() {
        test(0.7, 40);
    }

    @Test
    public void test10() {
        test(1, 100);
    }

    private void test(double input, int expectedOutput) {
        int output = TrackGradeAspect.applyGradeBottom(input, TrackGradeAspect.DESIRED_TEMPO_PROGRESS);
        Assert.assertEquals(expectedOutput, output);
    }

}