package com.nplekhanov.musix.model;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by nplekhanov on 21/02/2017.
 */
public class Database {
    private Collection<User> users;
    private transient Map<String, User> indexedUsers;
    private transient Map<String, Band> indexedBands;
    private Collection<Band> bands;
    private String defaultBand;
    private Collection<UserEvent> events = new ArrayList<>();
    private Collection<Track> tracks;

    public Collection<Track> getTracks() {
        return tracks;
    }

    public void setTracks(Collection<Track> tracks) {
        this.tracks = tracks;
    }

    public Collection<UserEvent> getEvents() {
        return events;
    }

    public void setEvents(Collection<UserEvent> events) {
        this.events = events;
    }

    public Map<String,User> indexedUsers() {
        if (indexedUsers == null) {
            indexedUsers = users.stream()
                    .collect(Collectors.toMap(User::getUid, x -> x));
        }
        return indexedUsers;
    }

    public Map<String, Track> indexedTracks() {
        return tracks.stream()
                .collect(Collectors.toMap(Track::getUuid, x -> x));
    }

    public Map<String,Band> indexedBands() {
        if (indexedBands == null) {
            indexedBands = bands.stream()
                    .collect(Collectors.toMap(Band::getName, x -> x));
        }
        return indexedBands;
    }

    public Collection<User> getUsers() {
        return users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }

    public Collection<Band> getBands() {
        return bands;
    }

    public void setBands(Collection<Band> bands) {
        this.bands = bands;
    }

    public String getDefaultBand() {
        return defaultBand;
    }

    public void setDefaultBand(String defaultBand) {
        this.defaultBand = defaultBand;
    }

    public void addEventAndGenerateIdAndDate(UserEvent event) {
        event.setTime(ZonedDateTime.now());
        event.setEventId(events.size() + 1);
        events.add(event);
    }
}
