<%@ tag pageEncoding="utf-8" %>
<%@ taglib prefix="mx" tagdir="/WEB-INF/tags" %>
<%@ attribute name="value" type="java.lang.String" required="true" %>
<%@ attribute name="name" type="java.lang.String" required="true" %>
<%@ tag import="java.util.List" %>
<%@ tag import="java.util.ArrayList" %>
<%!
    List<String> tunings = collectTunings();

    List<String> collectTunings() {
        List<String> tunings = new ArrayList<>();
        tunings.add("");
//        tunings.add("Std A");
//        tunings.add("Std A#");
        tunings.add("Std B");
        tunings.add("Std C");
        tunings.add("Std C#");
        tunings.add("Std D");
        tunings.add("Std D#");
        tunings.add("Std E");
//        tunings.add("Std F");
//        tunings.add("Std F#");
//        tunings.add("Std G");
//        tunings.add("Std G#");
//        tunings.add("Drop A");
//        tunings.add("Drop A#");
        tunings.add("Drop B");
        tunings.add("Drop C");
        tunings.add("Drop C#");
        tunings.add("Drop D");
        tunings.add("Drop D#");
//        tunings.add("Drop E");
//        tunings.add("Drop F");
//        tunings.add("Drop F#");
//        tunings.add("Drop G");
//        tunings.add("Drop G#");
        return tunings;
    }
%>
<select name="<%=name%>">
    <%
        for (String tuning : tunings) {
            %> <option <%=tuning.equals(value) ? "selected" : ""%>><%=tuning%></option> <%
        }
    %>
</select>