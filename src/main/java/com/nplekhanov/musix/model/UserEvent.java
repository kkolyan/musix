package com.nplekhanov.musix.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.time.ZonedDateTime;

/**
 * Created by nplekhanov on 07/12/2017.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = TrackRatedEvent.class, name = "TrackRated"),
        @JsonSubTypes.Type(value = TrackAddedEvent.class, name = "TrackAdded"),
        @JsonSubTypes.Type(value = TrackLinkAddedEvent.class, name = "TrackLinkAdded"),
        @JsonSubTypes.Type(value = TrackRatingChangedEvent.class, name = "TrackRatingChanged"),
        @JsonSubTypes.Type(value = TrackCommentChangedEvent.class, name = "TrackCommentChanged"),
        @JsonSubTypes.Type(value = TrackAttitudeChangedEvent.class, name = "TrackAttitudeChanged"),
        @JsonSubTypes.Type(value = TrackUpdatedEvent.class, name = "TrackUpdated"),
})
public abstract class UserEvent {
    private long eventId;
    private ZonedDateTime time;
    private String user;

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public ZonedDateTime getTime() {
        return time;
    }

    public void setTime(ZonedDateTime time) {
        this.time = time;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
