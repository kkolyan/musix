<%@ tag pageEncoding="utf-8" %>
<%@ tag import="org.apache.commons.lang3.StringUtils" %>
<%@ taglib prefix="mx" tagdir="/WEB-INF/tags" %>
<%@ attribute name="trackInfo" type="com.nplekhanov.musix.model.Track" required="true" %>
<%@ tag import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ tag import="java.net.URLEncoder" %>

<%!
    private String linkName(String link) {
        if (link.contains("youtube.com") || link.contains("youtu.be")) {
            return "YouTube";
        }
        if (link.contains("music.ya")) {
            return "Яндекс.Музыка";
        }
        if (link.contains("vk.com")) {
            return "Вконтакте";
        }
        return "link";
    }

    private String linkImage(String link) {
        if (link.contains("youtube.com") || link.contains("youtu.be")) {
            return "youtube.png";
        }
        if (link.contains("music.ya")) {
            return "yandex.png";
        }
        if (link.contains("vk.com")) {
            return "vk.png";
        }
        return null;
    }
%>
<%
    if (trackInfo != null) {
        String href = "http://google.com/search?q="+ URLEncoder.encode(trackInfo.getTrackName(), "UTF-8");
        %> <a target="_blank" href="<%=escapeHtml4(href)%>" title="Google it"> <img src="google.png"/></a> <%
        if (StringUtils.isNotBlank(trackInfo.getLink())) {
            for (String link : trackInfo.getLink().split("\\n")) {
                if (StringUtils.isBlank(link)) {
                    continue;
                }
                String image = linkImage(link);
                if (image == null) {
                        %> <a target="_blank" href="<%=escapeHtml4(link)%>"><%=linkName(link) %></a> <%
                } else {
                    %> <a target="_blank" href="<%=escapeHtml4(link)%>" title="<%=linkName(link)%>"> <img src = "<%=escapeHtml4(image)%>" /></a> <%
                }
            }
        }
    }
%>