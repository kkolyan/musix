package com.nplekhanov.musix;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by nplekhanov on 27/12/2017.
 */
public class By {
    public static <T, V> T getBy(Collection<T> collection, Function<T, V> extractor, V value) {
        List<T> list = collection.stream().filter(x -> Objects.equals(extractor.apply(x), value)).collect(Collectors.toList());
        if (list.size() != 1) {
            throw new IllegalStateException(""+list);
        }
        return list.get(0);
    }
}
