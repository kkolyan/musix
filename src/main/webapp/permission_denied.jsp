<%@ page import="com.nplekhanov.musix.AuthException" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String userId = (String) session.getAttribute("userId");
    if (userId == null) {
        throw new AuthException();
    }
%>
<div>You are not member. Please contact administrator and send him your user ID: <%= userId %>.</div>
<a href="Logout">Logout</a>