package com.nplekhanov.musix;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.nplekhanov.musix.model.Database;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by nplekhanov on 5/14/2017.
 */
public abstract class JsonRepository implements Repository {

    private final ThreadLocal<Database> threadLocalInChage = new ThreadLocal<>();

    protected final LockingExecutor lock;

    protected JsonRepository(LockingExecutor lock) {
        this.lock = lock;
    }

    public ObjectMapper createObjectMapper() {
        return new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public void change(UsersChange callback) {
        try {
            lock.withWriteLock(() -> {
                Database inChange = threadLocalInChage.get();
                if (inChange != null) {
                    callback.tryChange(inChange);
                    return null;
                }
                ObjectMapper objectMapper = createObjectMapper();
                Database db = objectMapper.readValue(readJson(), Database.class);

                this.threadLocalInChage.set(db);
                try {
                    boolean modified = callback.tryChange(db);

                    if (modified) {
                        db.setUsers(db.indexedUsers().values());
                        // to avoid inconsistent write serialization and write are split
                        String json = objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(db);
                        writeJsonAndMod(json);
                    }
                } finally {
                    this.threadLocalInChage.remove();
                }
                return null;
            });
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    protected abstract void writeJson(String json);

    private void writeJsonAndMod(String json) {
        writeJson(json);
        modCount.incrementAndGet();
    }

    @Override
    public <T> T read(UsersRead<T> callback) {
        try {
            return lock.withReadLock(() -> {
                Database db;
                ObjectMapper objectMapper = createObjectMapper();
                try {
                    String json = readJson();
                    db = objectMapper.readValue(json, Database.class);

                } catch (FileNotFoundException e) {
                    db = new Database();
                    db.setUsers(new ArrayList<>());
                }
                return callback.read(db);
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteDump(final String path) throws IOException {
        lock.withWriteLock(() -> {
            if (path == null || path.isEmpty()) {
                throw new IllegalArgumentException();
            }
            applyAction(path, (target, location) -> {
                if (location instanceof String) {
                    ((ObjectNode) target).remove((String) location);
                } else if (location instanceof Number) {
                    ArrayNode targetArray = (ArrayNode) target;
                    int targetIndex = ((Number) location).intValue();
                    targetArray.remove(targetIndex);
                } else throw new IllegalStateException("" + location);
            });
            return null;
        });
    }

    public void setDump(String path, String content, long expectedVersion) throws IOException {
        lock.withWriteLock(() -> {
            if (expectedVersion != modCount.get()) {
                throw new IllegalStateException("optimistic locking failed");
            }
            if (path == null || path.isEmpty()) {
                setDump(content);
            } else {
                applyAction(path, (target, location) -> {
                    ObjectMapper objectMapper = createObjectMapper();
                    JsonNode value = objectMapper.readValue(content, JsonNode.class);
                    if (location instanceof String) {
                        ((ObjectNode) target).replace((String) location, value);
                    } else if (location instanceof Number) {
                        ArrayNode targetArray = (ArrayNode) target;
                        int targetIndex = ((Number) location).intValue();
                        if (targetIndex == targetArray.size()) {
                            targetArray.add(value);
                        } else {
                            targetArray.set(targetIndex, value);
                        }
                    } else throw new IllegalStateException("" + location);
                });
            }
            return null;
        });
    }

    interface TerminalAction {
        void accept(JsonNode target, Object location) throws IOException;
    }

    private void applyAction(final String path, final TerminalAction terminalAction) throws IOException {
        ObjectMapper objectMapper = createObjectMapper();
        List<?> realPath = objectMapper.readValue(path, List.class);
        String json = readJson();
        JsonNode root = objectMapper.readValue(json, JsonNode.class);
        JsonNode target = root;
        for (Object segment : realPath.subList(0, realPath.size() - 1)) {
            if (segment instanceof String) {
                target = target.get((String) segment);
            } else if (segment instanceof Number) {
                target = target.get(((Number) segment).intValue());
            } else throw new IllegalStateException("" + segment);
        }
        Object location = realPath.get(realPath.size() - 1);
        terminalAction.accept(target, location);
        setDump(objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(root));
    }

    private void setDump(String content) {
        writeJsonAndMod(content);
    }

    private final AtomicLong modCount = new AtomicLong();

    public VersionBox<String> getDump(String path) throws IOException {
        return lock.withReadLock(() -> {
            if (path == null || path.isEmpty()) {
                return new VersionBox<>(modCount.get(), getDump());
            }
            JsonNode node = findNodeByPath(path);
            return new VersionBox<>(modCount.get(), createObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true).writeValueAsString(node));
        });
    }

    private String getDump() {
        try {
            return readJson();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public JsonNode findNodeByPath(String path) throws IOException {
        ObjectMapper objectMapper = createObjectMapper();
        List<?> realPath;
        if (path == null) {
            realPath = Collections.emptyList();
        } else {
            realPath = objectMapper.readValue(path, List.class);
        }
        String json = readJson();
        JsonNode value = objectMapper.readValue(json, JsonNode.class);

        if (path == null || path.isEmpty()) {
            return value;
        }
        for (Object segment : realPath) {
            if (segment instanceof String) {
                value = value.get((String) segment);
            } else if (segment instanceof Number) {
                value = value.get(((Number) segment).intValue());
            } else throw new IllegalStateException("" + segment);
        }
        return value;
    }

    protected abstract String readJson() throws IOException;

    public String encodePath(List<?> path) throws JsonProcessingException {
        return createObjectMapper().writeValueAsString(path);
    }
}
