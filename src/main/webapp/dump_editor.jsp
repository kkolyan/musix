<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="com.nplekhanov.musix.RepositoryUsingMusix" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="com.nplekhanov.musix.Repository" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="com.nplekhanov.musix.model.User" %>
<%@ page import="com.nplekhanov.musix.JsonRepository" %>
<%@ page import="com.fasterxml.jackson.databind.JsonNode" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="mx" tagdir="/WEB-INF/tags" %>
<%
    String userId = (String) session.getAttribute("userId");
    Musix musix = (Musix) application.getAttribute("musix");
    User currentUser = musix.getUser(userId);
    if (currentUser == null || !currentUser.isAdmin()) {
        response.sendError(503);
        return;
    }
    JsonRepository repository = (JsonRepository) application.getAttribute("repository");
    String path = request.getParameter("path");

    JsonNode node = repository.findNodeByPath(path);

%>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <style>
            .dump .node {
                padding-top: 3px;
                padding-bottom: 3px;
                padding-left: 10px;
                margin-left: 10px;
                border-left: 2px solid #ccc;
                border-bottom: 2px solid #ccc;
            }

            .dump .field_title {
                border-bottom: 2px solid #ccc;
                font-family: "Courier New", monospace;
            }
            .dump .field_name {
                font-weight: bold;
                cursor: pointer;
                color: #ae0d89;
            }
            .dump .preview {
                color: #9c9c9c;
            }

            .dump .bracket {
                font-weight: bold;
                color: #9c9c9c;
            }

            .dump .text {
                color: #00700b;
                font-family: "Courier New", monospace;
            }

            .dump .number {
                color: blue;
                font-family: "Courier New", monospace;
            }

            .dump .boolean, .dump .null {
                color: #0000ae;
                font-weight: bold;
                font-family: "Courier New", monospace;
            }

            .dump p.bracket  {
                display: none;
                padding-top: 0;
                padding-bottom: 0;
                margin-top: 0;
                margin-bottom: 0;
            }

        </style>
        <script>
            $(document).ready(function() {
                $(".field_name").click(function() {
                    $(this).parent().parent().next(".toggleable").toggle();
                });
                $(".fold_all").click(function() {
                    $(".toggleable").hide();
                });
                $(".expand_all").click(function() {
                    $(".toggleable").show();
                });
                $(".toggleable").hide();
            });
        </script>
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <a href="javascript:" class="fold_all">Fold All</a>
        <a href="javascript:" class="expand_all">Expand All</a>
        <mx:dump_node value="<%=node %>"/>
    </body>
</html>