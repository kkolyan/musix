package com.nplekhanov.musix.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by nplekhanov on 5/13/2017.
 */
public class Opinion {
    public static final int MAX_RATING = 5;
    private Attitude attitude;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean notEnoughSkills;
    
    private String comment;
    private long rating;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean rejectToPerform;

    public boolean isRejectToPerform() {
        return rejectToPerform;
    }

    public void setRejectToPerform(final boolean rejectToPerform) {
        this.rejectToPerform = rejectToPerform;
    }

    public long getRating() {
        return rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public Attitude getAttitude() {
        return attitude;
    }

    public void setAttitude(Attitude attitude) {
        this.attitude = attitude;
    }

    public boolean isNotEnoughSkills() {
        return notEnoughSkills;
    }

    public void setNotEnoughSkills(boolean notEnoughSkills) {
        this.notEnoughSkills = notEnoughSkills;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
