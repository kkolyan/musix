
$(document).ready(function() {
    window.scrollTo(0, location.hash.substr(1));
});

function updateScrollState(form) {
    var scrollState = $(form).find('input[name="scrollState"]');
    scrollState.val(window.scrollY);
    return true;
}