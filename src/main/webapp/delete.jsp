<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Musix musix = (Musix) application.getAttribute("musix");
    String track = request.getParameter("track");
    if (request.getMethod().equalsIgnoreCase("POST")) {
        String userId = (String) session.getAttribute("userId");
        musix.deleteTrack(userId, track);
        response.sendRedirect("opinions.jsp");
    } else {
        %>
<jsp:include page="header.jsp"/>
<div>
    <br/>
    Вы уверены, что хотите удалить трек <span style="font-weight: bold"><%=StringEscapeUtils.escapeHtml4(musix.getTrackInfo().get(track).getTrackName())%></span>?
    <form method="post">
        <input type="hidden" name="track" value="<%=StringEscapeUtils.escapeHtml4(track)%>">
        <input type="submit" value="Удалить"/>
    </form>
</div>

        <%
    }
%>