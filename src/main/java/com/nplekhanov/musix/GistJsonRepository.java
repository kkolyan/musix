package com.nplekhanov.musix;

import com.google.gson.Gson;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.Collections.singletonMap;

public class GistJsonRepository extends JsonRepository {

    public static final String TYPE = "gist";
    private final String gistId;
    private final String file;
    private final String token;

//    private final CloseableHttpClient client;

    public GistJsonRepository(LockingExecutor.ContextInfoProvider contextInfoProvider) {
        super(new LockingExecutor(contextInfoProvider, new File(File.listRoots()[0], "tmp/musix.lock")));
        gistId = System.getProperty("musix.repository.gist.id");
        file = System.getProperty("musix.repository.gist.file");
        token = System.getProperty("musix.repository.gist.token");
//        client = HttpClientBuilder.create().build();
    }

    @Override
    protected void writeJson(String json) {
        LinkedHashMap<Object, Object> wrapper = new LinkedHashMap<>();
        wrapper.put("files", singletonMap(file, singletonMap("content", json)));
        wrapper.put("description", "update " + Instant.now());
        wrapper.put("gist_id", gistId);
        try {
            HttpPatch patch = new HttpPatch("https://api.github.com/gists/" + gistId);
            patch.addHeader("Accept", "application/vnd.github+json");
            patch.addHeader("Authorization", "token " + token);
            patch.setEntity(new StringEntity(new Gson().toJson(wrapper), ContentType.APPLICATION_JSON));
            CloseableHttpResponse response = getClient().execute(patch);
            if (response.getStatusLine().getStatusCode() == 200) {
                return;
            }
            throw new IllegalStateException("patch failed: " + response);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected String readJson() throws IOException {
        CloseableHttpResponse response = getClient().execute(new HttpGet("https://api.github.com/gists/" + gistId));
        if (response.getStatusLine().getStatusCode() == 200) {
            Map<?,?> wrapper = new Gson().fromJson(new InputStreamReader(response.getEntity().getContent()), Map.class);
            Map<?, ?> files = (Map<?, ?>) wrapper.get("files");
            Map<?, ?> file = (Map<?, ?>) files.get(this.file);
            return (String) file.get("content");
        }
        throw new IllegalStateException("patch failed: " + response);
    }

    @Override
    public void close() {
        try {
            getClient().close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private CloseableHttpClient getClient() {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        return client;
    }
}
