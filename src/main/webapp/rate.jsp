<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="com.nplekhanov.musix.model.Attitude" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.*" %>
<%@ page import="com.nplekhanov.musix.model.Opinion" %>
<%@ page import="com.nplekhanov.musix.model.User" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.nplekhanov.musix.model.Track" %>
<%@ page import="java.util.UUID" %>
<%@ page import="static org.apache.commons.lang3.ObjectUtils.defaultIfNull" %>
<%@ page import="static org.apache.commons.lang3.ObjectUtils.defaultIfNull" %>
<%@ taglib prefix="mx" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="mobile.jsp"/>
    <title>Musix</title>
    <link rel="stylesheet" href="common.css" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
    </script>
</head>
<body>
<jsp:include page="header.jsp"/>
<%!
    private String attitudeLabel(Attitude a) {
        switch (a) {
            case UNDEFINED:
                return "Не определился";
            case DESIRED:
                return "Поддерживаю";
            case ACCEPTABLE:
                return "Согласен";
            case UNACCEPTABLE:
                return "Протестую";
        }
        throw new IllegalArgumentException(a+"");
    }
%>
<%

    String userId = (String) session.getAttribute("userId");
    Musix musix = (Musix) application.getAttribute("musix");
    User user = musix.getUser(userId);

    String track = request.getParameter("track");

    Track trackInfo = musix.getTrackInfo(track);
    Attitude lastAttitude = Attitude.UNDEFINED;
    String lastComment = "";
    boolean lastRejectToPerform = false;
    boolean lastNotEnoughSkills = false;
    long lastRating = 0;
    String tuningOriginal = "";
    String tuningTarget = "";

    if (trackInfo != null) {
        Opinion lastOpinion = trackInfo.getOpinionByUser().get(userId);
        if (lastOpinion != null) {
            lastAttitude = lastOpinion.getAttitude();
            lastComment = lastOpinion.getComment();
            lastNotEnoughSkills = lastOpinion.isNotEnoughSkills();
            lastRating = lastOpinion.getRating();
            lastRejectToPerform = lastOpinion.isRejectToPerform();
        }
        tuningOriginal = trackInfo.getTuningOriginal();
        tuningTarget = trackInfo.getTuningTarget();
    }
%>
<%
    String trackUuid = track != null ? track : UUID.randomUUID().toString().replace("-", "");%>

    <div>
        <form action="Rate" method="post">
            <%%>
            <input type="hidden" name="uuid" value="<%=escapeHtml4(trackUuid)%>"/>
            <br/>
            <label>
                Название трека
                <input style="width: 100%" type="text" name="trackName" value="<%=trackInfo == null ? "" : trackInfo.getTrackName()%>"/>
            </label>
            <br/>
            <div>
                <div><label>Строй (Оригинальный/Наш)</label></div>
                <mx:tuning_select value="<%=tuningOriginal%>" name="tuningOriginal"/>
                /
                <mx:tuning_select value="<%=tuningTarget%>" name="tuningTarget"/>
            </div>
            <br/>
            <div>
                <label for="listenLink">Ссылка на страницу, где можно послушать конкретную версию трека (Youtube, Last.FM и т.п.)</label>
                <div>
                    <textarea id="listenLink" name="listenLink" rows="3" cols="50"><%=trackInfo == null ? "" : escapeHtml4(trackInfo.getLink())%></textarea>
                </div>
                <%
                    if (trackInfo != null) for (String s : defaultIfNull(trackInfo.getLink(), "").split("\n")) {
                        s = s.trim();
                        if (s.isEmpty()) {
                            continue;
                        }
                        %> <div><a href="<%=escapeHtml4(s)%>" target="_blank"><%=escapeHtml4(s)%></a></div> <%
                    }
                %>
            </div>
            <br/>
            <div>
                <label for="tabLinks">Ссылки на табы/ноты</label>
                <div>
                <textarea id="tabLinks" name="tabLinks" rows="3" cols="50"><%=trackInfo == null ? "" : escapeHtml4(defaultIfNull(trackInfo.getTabLinks(), ""))%></textarea>
                </div>
                <%
                    if (trackInfo != null) for (String s : defaultIfNull(trackInfo.getTabLinks(), "").split("\n")) {
                        s = s.trim();
                        if (s.isEmpty()) {
                            continue;
                        }
                        %> <div><a href="<%=escapeHtml4(s)%>" target="_blank"><%=escapeHtml4(s)%></a></div> <%
                    }
                %>
            </div>
            <br/>
            <b>Ваше отношение к треку</b>
            <div>
                <%
                    for (Attitude a: Attitude.values()) {
                %>
                <label class="radio-option">
                    <input type="radio" name="attitude" value="<%=a%>"
                            <% if (lastAttitude == a) {%> checked <%} %>
                    />
                    <span><%=attitudeLabel(a)%></span>
                </label>
                <%
                    }
                %>
                <div>

<%--                    <label>--%>
<%--                        <input type="checkbox" name="rejectToPerform" <%if (lastRejectToPerform) {%> checked <%}%>/>--%>
<%--                        Не хочу исполнять, но готов отдохнуть и послушать--%>
<%--                    </label>--%>
                </div>
                <div>
                    <label>
                        Комментарий
                        <br/>
                        <textarea name="comment" rows="3" cols="50"><%=escapeHtml4(lastComment)%></textarea>
                    </label>
                </div>
                <div>

                    <label>
                        Недостаточно навыка
                        <input type="checkbox" name="notEnoughSkills" <%if (lastNotEnoughSkills) {%> checked <%}%>/>
                    </label>
                </div>
                <div>
                    <label>
                        Звездочки (влияют на взаиморасположение в чарте для равных по общему рейтингу треков) <br/>
                        <script>
                            function addRating(amount) {
                                var ratingField = document.getElementsByName("rating")[0];
                                var rating = parseInt(ratingField.value);
                                if (rating + amount >= 0 && rating + amount <= <%=Opinion.MAX_RATING%>) {
                                    rating += amount;
                                    ratingField.value = rating;
                                    document.getElementById("rating-display").innerHTML = Array(rating + 1).join('*');
                                }
                            }
                        </script>
                        <button type="button" onclick="addRating(-1);">-</button>
                        <button type="button" onclick="addRating(1);">+</button>
                        <span id="rating-display" style="font-weight: bold;"></span>
                        <input type="hidden" name="rating" value="<%=lastRating%>"/>
                        <script>
                            addRating(0);
                        </script>
                    </label>
                </div>

            </div>
            <br/>
            <input type="submit" value="Сохранить"/>
        </form>
    </div>
    <%
%>

<% if (track != null && user.hasAdminRights()) {
    %>
    <a href="activate.jsp?track=<%=URLEncoder.encode(track, "utf-8")%>">активировать</a>
    <a href="delete.jsp?track=<%=URLEncoder.encode(track, "utf-8")%>">удалить</a>

<%
    }%>
</body>
</html>
