package com.nplekhanov.musix.model;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by nplekhanov on 2/18/2017.
 */
public class User {
    private String uid;
    private String fullName;
    private String photoUrl;
    private boolean admin;

    private Map<String, Metric> metrics = new TreeMap<>();

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean hasAdminRights() {
        return isAdmin();
    }

    public Map<String, Metric> getMetrics() {
        return metrics;
    }

    public void setMetrics(Map<String, Metric> metrics) {
        this.metrics = new TreeMap<>(metrics);
    }
}
