package com.nplekhanov.musix;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class GoogleDriveJsonRepository extends JsonRepository {
    public static final String TYPE = "google-drive";
    private final String fileId;
    private final String credentialsFile;

    private final GoogleCredentials credentials;

    private final HttpRequestInitializer requestInitializer;

    private final Drive service;

    public GoogleDriveJsonRepository(LockingExecutor.ContextInfoProvider contextInfoProvider) throws IOException {
        super(new LockingExecutor(contextInfoProvider, new java.io.File(java.io.File.listRoots()[0], "tmp/musix.lock")));
        fileId = System.getProperty("musix.repository.google-drive.fileId");
        credentialsFile = System.getProperty("musix.repository.google-drive.credentialsFile");
        credentials = GoogleCredentials
            .fromStream(Files.newInputStream(Paths.get(credentialsFile)))
            .createScoped(Arrays.asList(DriveScopes.all().toArray(new String[0])));
        requestInitializer = new HttpCredentialsAdapter(credentials);
        service = new Drive.Builder(new NetHttpTransport(), GsonFactory.getDefaultInstance(), requestInitializer)
            .setApplicationName("Musix Google Drive Database")
            .build();
    }

    @Override
    protected void writeJson(String json) {
        try {
            ByteArrayContent content = new ByteArrayContent("application/json", json.getBytes(StandardCharsets.UTF_8));
            service.files()
                .update(fileId, new File(), content)
                .execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected String readJson() throws IOException {
        try {
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            service.files()
                .get(fileId)
                .executeMediaAndDownloadTo(buf);
            return buf.toString("utf-8");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
