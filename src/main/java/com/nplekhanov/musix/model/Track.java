package com.nplekhanov.musix.model;

import java.util.Map;

/**
 * Created by nplekhanov on 18/01/2018.
 */
public class Track {
    private String uuid;
    private String trackName;
    private String link;
    private Map<String, Opinion> opinionByUser;
    private String tabLinks;

    private String tuningOriginal;
    private String tuningTarget;

    public Track() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Map<String, Opinion> getOpinionByUser() {
        return opinionByUser;
    }

    public void setOpinionByUser(final Map<String, Opinion> opinionByUser) {
        this.opinionByUser = opinionByUser;
    }


    public String getTabLinks() {
        return tabLinks;
    }

    public void setTabLinks(String tabLinks) {
        this.tabLinks = tabLinks;
    }

    public String getTuningOriginal() {
        return tuningOriginal;
    }

    public void setTuningOriginal(String tuningOriginal) {
        this.tuningOriginal = tuningOriginal;
    }

    public String getTuningTarget() {
        return tuningTarget;
    }

    public void setTuningTarget(String tuningTarget) {
        this.tuningTarget = tuningTarget;
    }
}
