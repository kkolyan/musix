package com.nplekhanov.musix;

import com.nplekhanov.musix.model.User;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by nplekhanov on 2/18/2017.
 */
public class JsonFsRepositoryPopulator implements ServletContextListener, ServletRequestListener {

    private final ThreadLocal<String> contextInfo = new ThreadLocal<>();

    private JsonRepository resolveRepository(LockingExecutor.ContextInfoProvider contextInfoProvider) {
        String impl = System.getProperty("musix.repository", FileJsonRepository.TYPE);
        if (impl.equals(GoogleDriveJsonRepository.TYPE)) {
            try {
                return new GoogleDriveJsonRepository(contextInfoProvider);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        if (impl.equals(GistJsonRepository.TYPE)) {
            return new GistJsonRepository(contextInfoProvider);
        }
        if (impl.equals(FileJsonRepository.TYPE)) {
            return new FileJsonRepository(contextInfoProvider);
        }
        throw new IllegalStateException("unknown musix repository type: " + impl);
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        JsonRepository repository = resolveRepository(contextInfo::get);
        RepositoryUsingMusix musix = new RepositoryUsingMusix(repository);
        musix.init();
        servletContextEvent.getServletContext().setAttribute("repository", repository);
        servletContextEvent.getServletContext().setAttribute("musix", musix);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        JsonRepository repository = (JsonRepository) servletContextEvent.getServletContext().getAttribute("repository");
        if (repository != null) {
            repository.close();
        }
    }

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
        contextInfo.remove();
    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        HttpServletRequest servletRequest = (HttpServletRequest) servletRequestEvent.getServletRequest();
        String userId = (String) servletRequest.getSession().getAttribute("userId");
        Musix musix = (Musix) servletRequestEvent.getServletContext().getAttribute("musix");
        if (musix != null) {
            User user = musix.getUser(userId);
            if (user != null) {
                contextInfo.set("[" + userId + ", " + user.getFullName() + "]");
            } else {
                contextInfo.set("[" + userId + ", unavailable]");
            }
        } else {
            contextInfo.set("[" + userId + ", unavailable]");
        }
    }
}
