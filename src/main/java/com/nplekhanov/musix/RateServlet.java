package com.nplekhanov.musix;

import com.nplekhanov.musix.model.Attitude;
import com.nplekhanov.musix.model.Opinion;
import com.nplekhanov.musix.model.Track;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by nplekhanov on 2/19/2017.
 */
public class RateServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Musix musix = (Musix) req.getServletContext().getAttribute("musix");
        String userId = (String) req.getSession().getAttribute("userId");
        String uuid = req.getParameter("uuid");
        String trackName = req.getParameter("trackName");
        String notEnoughSkills = req.getParameter("notEnoughSkills");
        String comment = req.getParameter("comment");
        String attitude = req.getParameter("attitude");
        String rating = req.getParameter("rating");
        String tuningOriginal = req.getParameter("tuningOriginal");
        String tuningTarget = req.getParameter("tuningTarget");
        String tabLinks = req.getParameter("tabLinks");
        String rejectToPerform = req.getParameter("rejectToPerform");

        if (!musix.isFromDefaultBand(userId)) {
            resp.sendError(403);
            return;
        }
        Opinion opinion = new Opinion();
        opinion.setAttitude(Attitude.valueOf(attitude));
        opinion.setRating(Long.parseLong(rating));
        opinion.setComment(comment);
        opinion.setNotEnoughSkills("on".equals(notEnoughSkills));
        opinion.setRejectToPerform("on".equals(rejectToPerform));
        musix.addOpinion(userId, uuid, trackName, opinion);

        String listenLink = req.getParameter("listenLink");
        musix.addListenLink(userId, uuid, listenLink);

        musix.updateTrack(
            userId,
            uuid,
            new TrackUpdate<>("{Original tuning: %s}", Track::getTuningOriginal, Track::setTuningOriginal, emptyToNull(tuningOriginal)),
            new TrackUpdate<>("{Target tuning: %s}", Track::getTuningTarget, Track::setTuningTarget, emptyToNull(tuningTarget)),
            new TrackUpdate<>("{Tab/Notes links: %s}", Track::getTabLinks, Track::setTabLinks, emptyToNull(tabLinks))
        );

        resp.sendRedirect("opinions.jsp");
    }

    private static String emptyToNull(String s) {
        return s == null || s.isEmpty() ? null : s;
    }
}
