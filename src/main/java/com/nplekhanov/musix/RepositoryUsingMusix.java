package com.nplekhanov.musix;

import com.nplekhanov.musix.model.*;
import com.nplekhanov.musix.model.active.ActiveTrackOpinion;
import com.nplekhanov.musix.model.active.TrackGradeAspect;
import org.apache.commons.lang3.StringUtils;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by nplekhanov on 2/18/2017.
 */
public class RepositoryUsingMusix implements Musix {

    private Repository repository;

    public RepositoryUsingMusix(Repository repository) {
        this.repository = repository;
    }

    public void addUser(String userId, String fullName, String photoUrl) {
        repository.change(db -> {
            int changes = 0;
            User user = db.indexedUsers().get(userId);
            if (user == null) {
                user = new User();
                user.setUid(userId);
                db.indexedUsers().put(userId, user);
                changes++;
            }
            if (fullName != null && !fullName.trim().isEmpty() && !fullName.equals("null null") && !fullName.equals(user.getFullName())) {
                user.setFullName(fullName);
                changes++;
            }
            if (photoUrl != null && !photoUrl.isEmpty() && !photoUrl.equals(user.getPhotoUrl())) {
                user.setPhotoUrl(photoUrl);
                changes++;
            }
            return changes > 0;
        });
    }

    @Override
    public Map<String, User> getDefaultBand() {
        return repository.read(db -> db.indexedBands().get(db.getDefaultBand()).getMembers().stream()
                .map(x -> db.indexedUsers().get(x))
                .collect(Collectors.toMap(user -> user.getUid(), user -> user)));
    }

    @Override
    public TrackSorting getDefaultSorting() {
        return repository.read(db -> db.indexedBands().get(db.getDefaultBand()).getDefaultSorting());
    }

    @Override
    public Map<String, ActiveTrack> getActiveTracks() {
        return repository.read(db -> db.indexedBands().get(db.getDefaultBand()).getActiveTracks()).stream()
                .collect(Collectors.toMap(ActiveTrack::getUuid, x -> x));
    }

    public User getUser(String userId) {
        return repository.read(db -> db.indexedUsers().get(userId));
    }

    @Override
    public void addOpinion(String userId, String trackUuid, String trackName, Opinion opinion) {
        repository.change(db -> {
            Track track = db.indexedTracks().get(trackUuid);
            if (track == null) {
                track = new Track();
                track.setOpinionByUser(new LinkedHashMap<>());
                track.setUuid(trackUuid);
                db.getTracks().add(track);

                TrackAddedEvent trackAddedEvent = new TrackAddedEvent();
                trackAddedEvent.setTrackUuid(trackUuid);
                trackAddedEvent.setUser(userId);
                trackAddedEvent.setTrackName(trackName);
                db.addEventAndGenerateIdAndDate(trackAddedEvent);
            }
            track.setTrackName(trackName);
            Opinion last = db.indexedTracks().get(trackUuid.trim()).getOpinionByUser().put(userId, opinion);

            if (last == null ? opinion.getRating() > 0 : last.getRating() != opinion.getRating()) {
                TrackRatingChangedEvent ratingChangeEvent = new TrackRatingChangedEvent();
                ratingChangeEvent.setUser(userId);
                ratingChangeEvent.setTrackUuid(trackUuid);
                ratingChangeEvent.setTrackName(trackName);
                ratingChangeEvent.setRating(opinion.getRating());
                db.addEventAndGenerateIdAndDate(ratingChangeEvent);
            }

            if (last == null || last.getAttitude() != opinion.getAttitude()) {
                TrackAttitudeChangedEvent attitudeChangedEvent = new TrackAttitudeChangedEvent();
                attitudeChangedEvent.setUser(userId);
                attitudeChangedEvent.setTrackUuid(trackUuid);
                attitudeChangedEvent.setTrackName(trackName);
                attitudeChangedEvent.setAttitude(opinion.getAttitude());
                db.addEventAndGenerateIdAndDate(attitudeChangedEvent);
            }

            if ((last == null ?
                    StringUtils.isNotBlank(opinion.getComment()) :
                    !Objects.equals(normilizeEmptyText(last.getComment()), normilizeEmptyText(opinion.getComment())))) {
                TrackCommentChangedEvent commentChangedEvent = new TrackCommentChangedEvent();
                commentChangedEvent.setUser(userId);
                commentChangedEvent.setTrackUuid(trackUuid);
                commentChangedEvent.setTrackName(trackName);
                commentChangedEvent.setComment(opinion.getComment());
                db.addEventAndGenerateIdAndDate(commentChangedEvent);
            }

            return true;
        });
    }

    private String normilizeEmptyText(String text) {
        if (text == null || text.trim().isEmpty()) {
            return null;
        }
        return text;
    }

    public void init() {
    }

    @Override
    public List<Map.Entry<String, Opinion>> getOrderedTracks(String userId) {
        return repository.read(db -> {
            Comparator<Map.Entry<String, Opinion>> cmp = Comparator.comparing(x -> x.getValue().getAttitude().ordinal());
            cmp = cmp.thenComparing(x -> -x.getValue().getRating());
            cmp = cmp.thenComparing(Map.Entry::getKey);

            return db.getTracks().stream()
                    .map(track -> new AbstractMap.SimpleEntry<>(
                            track.getUuid(),
                            track.getOpinionByUser().get(userId)
                    ))
                    .filter(trackToOpinion -> trackToOpinion.getValue() != null)
                    .sorted(cmp)
                    .collect(Collectors.toList());
        });
    }

    @Override
    public void setTrackRating(final String userId, final String trackUuid, final int rating) {
        repository.change(db -> {
            Opinion opinion = db.indexedTracks().get(trackUuid).getOpinionByUser().get(userId);
            if (opinion.getRating() == rating) {
                return false;
            }
            opinion.setRating(rating);
            fixRating(opinion);
            return true;
        });
    }

    @Override
    public void increaseTrackOrder(String userId, String trackUuid, int step) {
        repository.change(db -> {
            Opinion opinion = db.indexedTracks().get(trackUuid).getOpinionByUser().get(userId);
            opinion.setRating(opinion.getRating() + step);
            fixRating(opinion);
            return true;
        });
    }

    @Override
    public void increaseTrackOrderOfDesired(String userId, int step) {
        repository.change(db -> {
            for (final Track track : db.getTracks()) {
                Opinion o = track.getOpinionByUser().get(userId);
                if (o != null && o.getAttitude() == Attitude.DESIRED) {
                    o.setRating(o.getRating() + step);
                    fixRating(o);
                }
            }
            return true;
        });
    }

    @Override
    public void increaseTrackOrderOfAll(String userId, int step) {
        repository.change(db -> {
            for (final Track track : db.getTracks()) {
                Opinion o = track.getOpinionByUser().get(userId);
                if (o != null && (o.getAttitude() == Attitude.DESIRED || o.getAttitude() == Attitude.ACCEPTABLE)) {
                    o.setRating(o.getRating() + step);
                    fixRating(o);
                }
            }
            return true;
        });
    }

    @Override
    public boolean isFromDefaultBand(String userId) {
        return repository.read(db -> db.indexedBands().get(db.getDefaultBand()).getMembers().contains(userId));
    }

    private void fixRating(Opinion o) {
        if (o.getRating() < 0) {
            o.setRating(0);
        }
        if (o.getRating() > 5) {
            o.setRating(5);
        }
    }

    @Override
    public void activateTrack(String trackUuid) {
        repository.change(db -> {
            Band band = db.indexedBands().get(db.getDefaultBand());
            List<User> members = db.getUsers().stream().filter(x -> band.getMembers().contains(x.getUid())).collect(Collectors.toList());
            ActiveTrack at = new ActiveTrack();
            at.setUuid(trackUuid);
            at.setOpinionByUser(new HashMap<>());
            band.getActiveTracks().add(at);
            return true;
        });
    }

    @Override
    public Collection<? extends UserEvent> getEventsLastFirst() {
        return repository.read(Database::getEvents).stream()
                .sorted(Comparator.comparing(event -> -event.getEventId()))
                .collect(Collectors.toList());
    }

    @Override
    public void submitActiveTrackOpinion(String userId, String trackUuid, Integer newPriority, String comment, Map<TrackGradeAspect, Double> grades) {
        repository.change(db -> {
            ActiveTrack activeTrack = By.getBy(db.indexedBands().get(db.getDefaultBand()).getActiveTracks(), ActiveTrack::getUuid, trackUuid);
            Map<String, ActiveTrackOpinion> opinionByUser = activeTrack.getOpinionByUser();
            if (opinionByUser == null) {
                opinionByUser = new TreeMap<>();
                activeTrack.setOpinionByUser(opinionByUser);
            }
            if (newPriority != null) {
                activeTrack.setPriority(newPriority);
            }
            ActiveTrackOpinion trackOpinion = opinionByUser.computeIfAbsent(userId, k -> new ActiveTrackOpinion());
            trackOpinion.setComment(comment);
            trackOpinion.setGrades(grades);
            return true;
        });
    }

    @Override
    public void deleteTrack(String userId, String trackUuid) {
        repository.change(db -> {
            db.setTracks(db.getTracks().stream()
                    .filter(x -> !x.getUuid().equals(trackUuid))
                    .collect(Collectors.toList()));
            return true;
        });
    }

    @Override
    public Track getTrackInfo(String trackUuid) {
        Track trackInfo = repository.read(db -> {
            if (db.getTracks() == null) {
                db.setTracks(new ArrayList<>());
            }
            Map<String, Track> indexedTracks = db.indexedTracks();
            return indexedTracks.get(trackUuid);
        });
        return trackInfo;
    }

    @Override
    public Map<String, Track> getTrackInfo() {
        return repository.read(Database::indexedTracks);
    }

    @Override
    public void addListenLink(String userId, String trackUuid, String listenLink) {
        repository.change(db -> {
            if (db.getTracks() == null) {
                db.setTracks(new ArrayList<>());
            }

            Track trackInfo = db.indexedTracks().get(trackUuid);
            if (trackInfo == null) {
                throw new IllegalStateException("WTF");
            }
            if (Objects.equals(listenLink, trackInfo.getLink())) {
                return false;
            }
            if (listenLink.isEmpty() && trackInfo.getLink() == null) {
                return false;
            }
            trackInfo.setLink(listenLink);
            TrackLinkAddedEvent linkAdded = new TrackLinkAddedEvent();
            linkAdded.setUser(userId);
            linkAdded.setTrackUuid(trackUuid);
            linkAdded.setTrackName(trackInfo.getTrackName());
            linkAdded.setLink(listenLink);
            db.addEventAndGenerateIdAndDate(linkAdded);

            return true;
        });
    }

    @Override
    public void updateTrack(String userId, String trackUuid, TrackUpdate<?>... updates) {
        repository.change(db -> {
            if (db.getTracks() == null) {
                db.setTracks(new ArrayList<>());
            }

            Track trackInfo = db.indexedTracks().get(trackUuid);
            if (trackInfo == null) {
                throw new IllegalStateException("WTF");
            }
            boolean changed = false;
            for (TrackUpdate<?> update : updates) {
                TrackUpdatedEvent event = tryUpdate(trackInfo, update, userId);
                if (event != null) {
                    changed = true;
                    db.addEventAndGenerateIdAndDate(event);
                }
            }
            return changed;
        });
    }

    @Override
    public void addUserMetric(String userId, String... metrics) {
        repository.change(db -> {
            User user = db.indexedUsers().get(userId);
            if (user == null) {
                return false;
            }
            for (String metric : metrics) {
                Metric value = user.getMetrics()
                    .computeIfAbsent(metric, s -> new Metric());
                value.setTotal(value.getTotal() + 1);
            }
            return true;
        });
    }


    private <T> TrackUpdatedEvent tryUpdate(Track track, TrackUpdate<T> update, String user) {
        T currentValue = update.getGet().apply(track);
        if (Objects.equals(currentValue, update.getValue())) {
            return null;
        }
        update.getSet().accept(track, update.getValue());
        TrackUpdatedEvent event = new TrackUpdatedEvent();
        event.setDescription(String.format(update.getDescriptionPattern(), update.getValue()));
        event.setTrackName(track.getTrackName());
        event.setTrackUuid(track.getUuid());
        event.setUser(user);
        return event;
    }
}
