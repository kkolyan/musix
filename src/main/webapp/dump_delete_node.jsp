<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.nplekhanov.musix.JsonRepository" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    JsonRepository repository = (JsonRepository) application.getAttribute("repository");
    String path = request.getParameter("path");
    if (request.getMethod().equalsIgnoreCase("POST")) {
        repository.deleteDump(path);
        response.sendRedirect("dump_editor.jsp");
    } else {
        %>
<jsp:include page="header.jsp"/>
<div>
    <br/>
    Вы уверены, что хотите удалить свойство <span style="font-weight: bold"><%=StringEscapeUtils.escapeHtml4(path)%></span>?
    <form method="post">
        <input type="hidden" name="path" value="<%=StringEscapeUtils.escapeHtml4(URLEncoder.encode(path, "UTF-8"))%>">
        <input type="submit" value="Удалить"/>
    </form>
    <pre>
<%=StringEscapeUtils.escapeHtml4(repository.getDump(path))%>
    </pre>
</div>

        <%
    }
%>