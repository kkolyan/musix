package com.nplekhanov.musix;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class FileJsonRepository extends JsonRepository {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    public static final String TYPE = "file";

    public FileJsonRepository(LockingExecutor.ContextInfoProvider contextInfoProvider) {
        super(new LockingExecutor(contextInfoProvider, new File(file().getParentFile(), "musix.lock")));
        new Thread(() -> {
            try {
                while (true) {
                    doCheckWorkingDirectory();
                }
            } catch (InterruptedException e) {
                logger.warn("auto-push loop interrupted");
            }
        }).start();
    }

    private static File file() {
        return new File(ConfigFactory.getConfig().getRepositoryFilePath());
    }

    private void doCheckWorkingDirectory() throws InterruptedException {
        boolean[] failed = {false};
        try {
            lock.withWriteLock(() -> {
                try {
                    GitPusher.commitAndPushChanges(file().getParentFile());
                } catch (GitAPIException e) {
                    failed[0] = true;
                    logger.error("iteration failed: {}", e.toString(), e);
                }
                return null;
            });
        } catch (IOException e) {
            failed[0] = true;
            logger.error("iteration failed: {}", e.toString());
        }
        if (failed[0]) {
            Thread.sleep(60000);
        } else {
            Thread.sleep(30000);
        }
    }

    @Override
    protected void writeJson(String json) {
        try {
            FileUtils.write(file(), json, "UTF-8");

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ConfigFactory.getConfig().getBackupSuffixDateTimeFormat());

            File backupFile = new File(new File(ConfigFactory.getConfig().getRepositoryFilePath()).getParentFile(), "musix.backup" + formatter.format(OffsetDateTime.now()) + ".json");
            FileUtils.write(backupFile, json, "UTF-8");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected String readJson() throws IOException {
        try {
            return FileUtils.readFileToString(file(), "UTF-8");
        } catch (FileNotFoundException e) {
            return "{\"now\": \"2022-07-17T13:58:39.720Z\", \"bands\" : [{\"name\": \"band\", \"members\": [], \"activeTracks\": []}], \"defaultBand\": \"band\", \"users\": [], \"tracks\": []}\n";
        }
    }

    public String getGitInfo() throws IOException {
        return lock.withReadLock(() -> GitPusher.getGitInfo(file().getParentFile()));

    }
}
