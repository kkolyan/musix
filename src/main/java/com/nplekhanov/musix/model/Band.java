package com.nplekhanov.musix.model;

import com.nplekhanov.musix.TrackSorting;

import java.util.Collection;
import java.util.List;

/**
 * Created by nplekhanov on 4/29/2017.
 */
public class Band {
    private String name;
    private List<String> members;
    private Collection<ActiveTrack> activeTracks;
    private TrackSorting defaultSorting;

    public Collection<ActiveTrack> getActiveTracks() {
        return activeTracks;
    }

    public void setActiveTracks(Collection<ActiveTrack> activeTracks) {
        this.activeTracks = activeTracks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public TrackSorting getDefaultSorting() {
        return defaultSorting;
    }

    public void setDefaultSorting(TrackSorting defaultSorting) {
        this.defaultSorting = defaultSorting;
    }
}
