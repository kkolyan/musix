<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.nplekhanov.musix.model.*" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.time.ZoneId" %>
<%@ page import="java.time.ZoneOffset" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>События</title>
    <jsp:include page="mobile.jsp"/>
    <link rel="stylesheet" href="common.css" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .track {
            color: #7677a4;
            font-weight: bold;
        }
        .comment {
            color: #00700b;
            font-weight: bold;
        }
    </style>
</head>
<body>
<jsp:include page="header.jsp"/>
    <%

        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss Z");

        Musix musix = (Musix) application.getAttribute("musix");
        Map<String, User> band = musix.getDefaultBand();
        for (UserEvent event : musix.getEventsLastFirst()) {
            User user = band.get(event.getUser());
            if (user == null) {
                continue;
            }
            %>

            <div style="padding-left: 10px; padding-top: 10px">
                <span style="font-weight: bold; color: #9c9c9c">
                    <b style="color: #9c9c9c" title="<%=event.getEventId()%>"><%=escapeHtml4(event.getTime()
                            .toInstant()
                            .atOffset(ZoneOffset.ofHours(3))
                            .format(timeFormatter))%></b>
                    <b style="color: brown"><%=escapeHtml4(user.getFullName())%></b>
                </span>
                <%
                if (event instanceof TrackRatedEvent) {
                    TrackRatedEvent ev = (TrackRatedEvent) event;
                    if (ev.getOpinion().getAttitude() == Attitude.ACCEPTABLE) {
                        %> согласен <%
                    } else if (ev.getOpinion().getAttitude() == Attitude.DESIRED) {
                        %> хочет <%
                    } else if (ev.getOpinion().getAttitude() == Attitude.UNACCEPTABLE) {
                        %> отказывается <%
                    } else throw new IllegalStateException("unknown attitude: "+ev.getOpinion().getAttitude());
                    %> играть <span class="track"><%=escapeHtml4(ev.getTrackName())%></span><%
                    if (!StringUtils.isEmpty(ev.getOpinion().getComment())) {
                        %>: <span class="comment"><%=escapeHtml4(ev.getOpinion().getComment())%></span> <%
                    }
                } else if (event instanceof TrackAddedEvent) {
                    TrackAddedEvent ev = (TrackAddedEvent) event;
                    %>добавил трек <a href="rate.jsp?track=<%=ev.getTrackUuid()%>"><span class="track"><%=escapeHtml4((ev).getTrackName())%></span></a> <%
                } else if (event instanceof TrackLinkAddedEvent){
                    TrackLinkAddedEvent ev = (TrackLinkAddedEvent) event;
                    String link = ((TrackLinkAddedEvent) event).getLink();
                    if (link == null || link.isEmpty()) {
                        %>удалил ссылку на трек <span class="track"><%=escapeHtml4(ev.getTrackName())%></span> <%
                    } else {
                        %>изменил <a href="<%=escapeHtml4(ev.getLink())%>" target="_blank">ссылку</a> на трек <span class="track"><%=escapeHtml4(ev.getTrackName())%></span> <%
                    }
                } else if (event instanceof TrackAttitudeChangedEvent) {
                    TrackAttitudeChangedEvent ev = (TrackAttitudeChangedEvent) event;
                    if (ev.getAttitude() == Attitude.ACCEPTABLE) {
                    %> согласен <%
                    } else if (ev.getAttitude() == Attitude.UNDEFINED) {
                    %> думает <%
                    } else if (ev.getAttitude() == Attitude.DESIRED) {
                    %> хочет <%
                    } else if (ev.getAttitude() == Attitude.UNACCEPTABLE) {
                    %> отказывается <%
                    } else throw new IllegalStateException("unknown attitude: "+ev.getAttitude());
                    %> играть <a href="rate.jsp?track=<%=ev.getTrackUuid()%>"><span class="track"><%=escapeHtml4((ev).getTrackName())%></span></a></span><%
                } else if (event instanceof TrackCommentChangedEvent) {
                    TrackCommentChangedEvent ev = (TrackCommentChangedEvent) event;
                    if (StringUtils.isBlank(ev.getComment())) {
                        %>удалил(а) коментарий к треку <a href="rate.jsp?track=<%=ev.getTrackUuid()%>"><span class="track"><%=escapeHtml4((ev).getTrackName())%></span></a> <%
                    } else {
                        %>прокомментировал(а) трек <a href="rate.jsp?track=<%=ev.getTrackUuid()%>"><span class="track"><%=escapeHtml4((ev).getTrackName())%></span></a> <%
                        %>: <span class="comment"><%=escapeHtml4(ev.getComment())%></span> <%
                    }
                } else if (event instanceof TrackRatingChangedEvent) {
                    TrackRatingChangedEvent ev = (TrackRatingChangedEvent) event;
                    if (ev.getRating() > 0) {
                         %>дал <span class="rating"><%=StringUtils.repeat("*", (int) ev.getRating())%></span> треку <span class="track"><%=escapeHtml4(ev.getTrackName())%></span> <%
                    } else {
                         %>убрал все здвезды у трека <a href="rate.jsp?track=<%=ev.getTrackUuid()%>"><span class="track"><%=escapeHtml4((ev).getTrackName())%></span></a> <%
                    }
                } else if (event instanceof TrackUpdatedEvent) {
                    TrackUpdatedEvent ev = (TrackUpdatedEvent) event;
                    %>Изменил трек <a href="rate.jsp?track=<%=ev.getTrackUuid()%>"><span class="track"><%=escapeHtml4((ev).getTrackName())%></span></a>: <span class="comment"><%=escapeHtml4(ev.getDescription())%></span> <%
                } else throw new IllegalStateException("unsupported event type: "+event);
                %>
            </div>
            <%
        }
    %>
</body>
</html>
