package com.nplekhanov.musix.model;

import java.util.Collection;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created by nplekhanov on 5/13/2017.
 */
public class OpinionTrack {

    private Track track;
    private Map<String,Opinion> opinionByUser;
    private Map<User, Long> ratedWithinGroup;
    private long positionInsideGroup;
    private long globalPosition;
    private Map<String, Integer> groupResults;
    private transient int scoreWithinGroup;

    public long getPositionInsideGroup() {
        return positionInsideGroup;
    }

    public void setPositionInsideGroup(long positionInsideGroup) {
        this.positionInsideGroup = positionInsideGroup;
    }

    public Map<User, Long> getRatedWithinGroup() {
        return ratedWithinGroup;
    }

    public void setRatedWithinGroup(Map<User, Long> ratedWithinGroup) {
        this.ratedWithinGroup = ratedWithinGroup;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public Map<String, Opinion> getOpinionByUser() {
        return opinionByUser;
    }

    public void setOpinionByUser(Map<String, Opinion> opinionByUser) {
        this.opinionByUser = opinionByUser;
    }

    private Collection<String> getTracksByRelationWithinGroup(int relation) {
        return getGroupResults().entrySet().stream()
                .filter(x -> x.getValue() == relation)
                .map(Map.Entry::getKey)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    public Collection<String> getSuperiorWithinGroup() {
        return getTracksByRelationWithinGroup(-1);
    }

    public Collection<String> getInferiorWithinGroup() {
        return getTracksByRelationWithinGroup(1);
    }

    public Collection<String> getEqualWithinGroup() {
        return getTracksByRelationWithinGroup(0);
    }

    public Map<String, Integer> getGroupResults() {
        return groupResults;
    }

    public void setGroupResults(Map<String, Integer> groupResults) {
        this.groupResults = groupResults;
        this.scoreWithinGroup = groupResults.values().stream().mapToInt(x -> x).sum();
    }

    public int getScoreWithinGroup() {
        return scoreWithinGroup;
    }

    public long getGlobalPosition() {
        return globalPosition;
    }

    public void setGlobalPosition(long globalPosition) {
        this.globalPosition = globalPosition;
    }
}
