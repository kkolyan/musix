package com.nplekhanov.musix.model.active;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by nplekhanov on 26/12/2017.
 */
public class ActiveTrackOpinion {
    private String comment = "";
    private Map<TrackGradeAspect, Double> grades = new TreeMap<>();

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Map<TrackGradeAspect, Double> getGrades() {
        return grades;
    }

    public void setGrades(Map<TrackGradeAspect, Double> grades) {
        this.grades = grades;
    }
}
