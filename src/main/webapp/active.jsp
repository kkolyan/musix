<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ page import="com.nplekhanov.musix.model.Attitude" %>
<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="com.nplekhanov.musix.model.User" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.nplekhanov.musix.model.ActiveTrack" %>
<%@ page import="com.nplekhanov.musix.model.active.TrackGradeAspect" %>
<%@ page import="com.nplekhanov.musix.model.active.ActiveTrackOpinion" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Comparator" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Мнения</title>
    <jsp:include page="mobile.jsp"/>
    <link rel="stylesheet" href="common.css" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .comment-button:hover, .comment-button-all:hover {
            cursor: pointer;
        }

        img.cell-img {
            margin-top: -7px;
            margin-right: 0;
            margin-left: 0;
            margin-bottom: -8px;
            padding: 0;
        }

        .grade { font-weight: bold; }
        .grade-0 { color:   rgb(255,  0, 0); }
        .grade-10 { color:  rgb(225,  65, 0); }
        .grade-20 { color:  rgb(225,  90, 0); }
        .grade-30 { color:  rgb(200,  105, 0); }
        .grade-40 { color:  rgb(175, 105, 0); }
        .grade-50 { color:  rgb(150, 105, 0); }
        .grade-60 { color:  rgb(140, 110, 0); }
        .grade-70 { color:  rgb(130, 115, 0); }
        .grade-80 { color:  rgb(120, 120, 0); }
        .grade-90 { color:  rgb( 75, 127, 0); }
        .grade-100 { color: rgb(  0, 127, 0); }
        .grade-comment {
            font-weight: bold;
            color: #9c9c9c;
        }
    </style>
    <script>
        var showAllComments = false;

        $(document).ready(function() {
            $(".comment-button").click(function() {
                $(this).next().toggle();
            });
            $(".comment-button-all").click(function() {
                showAllComments = ! showAllComments;
                if (showAllComments) {
                    $(".comment").show();
                } else {
                    $(".comment").hide();
                }
            });
            $("#showDetails").click(function () {
                $(".details-short").hide();
                $(".details-full").show();
            });
            var hideDetails = function () {
                $(".details-short").show();
                $(".details-full").hide();
            };
            $("#hideDetails").click(hideDetails);
            hideDetails()
        });
    </script>
</head>
<style>
</style>
<%!
    private String attitudeLabel(Attitude a) {
        switch (a) {
            case UNDEFINED:
                return "Не определился";
            case DESIRED:
                return "Поддерживаю";
            case ACCEPTABLE:
                return "Согласен";
            case UNACCEPTABLE:
                return "Протестую";
        }
        throw new IllegalArgumentException(a+"");
    }

    private String trackGradeAspectLabel(TrackGradeAspect a) {
        switch (a) {
            case FUN_OF_PLAYING:
                return "fun";
            case READY_TO_STAGE_PROGRESS:
                return "stage";
            case READY_TO_WORK_PROGRESS:
                return "work";
            case DESIRED_TEMPO_PROGRESS:
                return "tempo";
            case ORIGINAL_BOTTLENECKS_PROGRESS:
                return "integrity";
        }
        throw new IllegalArgumentException(a+"");
    }

    private String trackGradeAspectExplanation(TrackGradeAspect a) {
        switch (a) {

            case FUN_OF_PLAYING:
                return "удовольствие от исполнения";
            case READY_TO_STAGE_PROGRESS:
                return "готовность к выступлению с этим треком";
            case READY_TO_WORK_PROGRESS:
                return "готовность работать над треком на репах";
            case DESIRED_TEMPO_PROGRESS:
                return "способность исполнять трек в нормальном темпе. в процентах от оригинального";
            case ORIGINAL_BOTTLENECKS_PROGRESS:
                return "процент элементов трека, которые готов исполнять без упрощенияю в темпе, указанном в предыдущем пункте. " +
                        "например: 0 - упрощаю все элементы, 75% - немного упрощаю шреддинг в конце песни или играю переменным штрихом вместо даунстрока в куплетах, 50% - и то и другое";
        }
        throw new IllegalArgumentException(a+"");
    }
%>
<%
    Musix musix = (Musix) application.getAttribute("musix");

    Collection<User> users = musix.getDefaultBand().values();
    String userId = (String) session.getAttribute("userId");
    User currentUser = musix.getUser(userId);

%>
<body>
<jsp:include page="header.jsp"/>
<a id="showDetails" class="details-short" href="javascript:">Показать детали</a>
<a id="hideDetails" class="details-full" href="javascript:">Скрыть детали</a>

<%
    String selectedTrack = request.getParameter("track");
    if (selectedTrack == null) {

        %>
        <table>
            <tr>
                <th>Трек</th>
                <th>Ссылка</th>
                <th>Приоритет</th>

                <%
                    for (User u: users) {
                %> <th> <img width="64" src="<%=u.getPhotoUrl()%>" title="<%=escapeHtml4(u.getFullName())%>"/> </th> <%
                }
            %>
            </tr>
            <%
                List<ActiveTrack> activeTracks = new ArrayList<ActiveTrack>(musix.getActiveTracks().values());
                Collections.sort(activeTracks, new Comparator<ActiveTrack>() {
                    @Override
                    public int compare(ActiveTrack o1, ActiveTrack o2) {
                        int cmp = -Integer.compare(o1.getPriority(), o2.getPriority());
                        if (cmp == 0) {
                            cmp = o1.getUuid().compareTo(o2.getUuid());
                        }
                        return cmp;
                    }
                });
                for (ActiveTrack track: activeTracks) {
                %>
                <tr>
                <th> <a href="active.jsp?track=<%=escapeHtml4(track.getUuid())%>"><%=escapeHtml4(track.getUuid())%></a> </th>
                <th><tags:track_links trackInfo="<%=musix.getTrackInfo(track.getUuid())%>"/> </th>
                <th><%=track.getPriority()%></th>
                <%
                    for (User user: users) {
                        %> <td><%
                    Map<String, ActiveTrackOpinion> opinionByUser = track.getOpinionByUser();
                    if (opinionByUser == null) {
                        opinionByUser = Collections.emptyMap();
                    }
                    ActiveTrackOpinion activeTrackOpinion = opinionByUser.get(user.getUid());
                        if (activeTrackOpinion != null) {
                            if (!StringUtils.isEmpty(activeTrackOpinion.getComment())) {
                                %> <div class="grade-comment"><%=escapeHtml4(activeTrackOpinion.getComment())%></div> <%
                            }
                            Double stage = activeTrackOpinion.getGrades().getOrDefault(TrackGradeAspect.READY_TO_STAGE_PROGRESS, 0.0);
                            %><span class="details-short grade grade-<%=TrackGradeAspect.applyGradeBottom(stage, TrackGradeAspect.READY_TO_STAGE_PROGRESS)%>"><%= stage * 100 %>%</span><%
                            %><div class="details-full"><%
                            for (TrackGradeAspect aspect: activeTrackOpinion.getGrades().keySet()) {
                                Double gradeValue = activeTrackOpinion.getGrades().get(aspect);
                                %> <span title="<%=trackGradeAspectExplanation(aspect)%>"><%=trackGradeAspectLabel(aspect)%></span>: <span class="details-full grade grade-<%=TrackGradeAspect.applyGradeBottom(gradeValue, aspect)%>"><%= gradeValue * 100 %>%</span> <br/> <%
                            }
                            %></div><%
                        }
                        %></td> <%
                    }
                %>
                </tr>
            <%
            }
        %>
        </table>

        <%
            for (TrackGradeAspect aspect: TrackGradeAspect.values()) {
                %><div> <b><%=trackGradeAspectLabel(aspect)%></b>: <%=trackGradeAspectExplanation(aspect)%> </div><%
            }
        %>
        <%
    } else {
        ActiveTrack activeTrack = musix.getActiveTracks().get(selectedTrack);
        if (activeTrack == null) {
            response.sendError(404);
            return;
        }
        Map<String, ActiveTrackOpinion> opinionByUser = activeTrack.getOpinionByUser();
        if (opinionByUser == null) {
            opinionByUser = Collections.emptyMap();
        }
        ActiveTrackOpinion activeTrackOpinion = opinionByUser.get(userId);
        if (activeTrackOpinion == null) {
            activeTrackOpinion = new ActiveTrackOpinion();
        }
        %>
        <h3>Мнение о <b><%=escapeHtml4(selectedTrack)%></b></h3>
        <% if (currentUser.hasAdminRights()) {
            %><tags:track_vote_link track="<%=musix.getTrackInfo().get(selectedTrack)%>"/><%
        }%>
        <form action="EditActiveTrackOpinion" method="post">
            <input type="hidden" name="track" value="<%=escapeHtml4(selectedTrack)%>"/>
            <% if (currentUser.hasAdminRights()) {  %>
                <label>
                    Приоритет
                    <input type="text" name="priority" value="<%=activeTrack.getPriority()%>"/>
                </label>
            <% }%>

            <%
            for (TrackGradeAspect aspect: TrackGradeAspect.values()) {
                Double gradeValue = activeTrackOpinion.getGrades().get(aspect);
                %>
                <p>
                    <label>
                        <%=trackGradeAspectExplanation(aspect)%>
                        <select name="<%=aspect.name()%>">
                            <option value="" <%if (gradeValue == null) { %> selected <%} %>> - </option>
                            <%
                                int step = 10;
                                int start = 0;
                                if (aspect.getBottom() > .4) {
                                    step = 5;
                                    start = (int) (aspect.getBottom() * 10) * 10;
                                }
                                for (int i = start; i <= 100; i += step) {
                                %> <option value="<%= i / 100.0 %>" <% if (gradeValue != null && gradeValue * 100 == i) {%> selected <%}%>> <%=i%>% </option> <%
                            }
                            %>
                        </select>
                    </label>
                </p>
                <%
            }
            %>
            <p>
                <label>
                    Коментарий<br/>
                    <textarea cols="100" rows="10" name="comment"><%=escapeHtml4(activeTrackOpinion.getComment())%></textarea>
                </label>
            </p>
            <input type="submit" value="Сохранить"/>
        </form>
        <%
    }
%>

</body>
</html>
