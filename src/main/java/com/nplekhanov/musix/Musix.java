package com.nplekhanov.musix;

import com.nplekhanov.musix.model.ActiveTrack;
import com.nplekhanov.musix.model.Opinion;
import com.nplekhanov.musix.model.Track;
import com.nplekhanov.musix.model.User;
import com.nplekhanov.musix.model.UserEvent;
import com.nplekhanov.musix.model.active.TrackGradeAspect;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by nplekhanov on 2/18/2017.
 */
public interface Musix {
    void addUser(String userId, String fullName, String photoUrl);

    Map<String, User> getDefaultBand();

    TrackSorting getDefaultSorting();

    Map<String, ActiveTrack> getActiveTracks();

    User getUser(String userId);

    void addOpinion(String userId, String trackUuid, String trackName, Opinion opinion);

    List<Map.Entry<String, Opinion>> getOrderedTracks(String userId);

    void setTrackRating(String userId, String trackUuid, int rating);

    void increaseTrackOrder(String userId, String trackUuid, int step);

    void increaseTrackOrderOfDesired(String userId, int step);

    void increaseTrackOrderOfAll(String userId, int step);

    boolean isFromDefaultBand(String userId);

    void activateTrack(String trackUuid);

    Collection<? extends UserEvent> getEventsLastFirst();

    void submitActiveTrackOpinion(String userId, String trackUuid, Integer newPriority, String comment, Map<TrackGradeAspect, Double> grades);

    void deleteTrack(String userId, String trackUuid);

    Track getTrackInfo(String trackUuid);

    Map<String, Track> getTrackInfo();

    void addListenLink(String userId, String trackUuid, String listenLink);
    void updateTrack(String userId, String trackUuid, TrackUpdate<?>... updates);

    void addUserMetric(String userId, String ...metrics);
}
