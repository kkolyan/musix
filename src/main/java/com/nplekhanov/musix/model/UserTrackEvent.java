package com.nplekhanov.musix.model;

public abstract class UserTrackEvent extends UserEvent {
    private String trackName;
    private String trackUuid;

    public String getTrackUuid() {
        return trackUuid;
    }

    public void setTrackUuid(String trackUuid) {
        this.trackUuid = trackUuid;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }
}
