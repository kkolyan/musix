package com.nplekhanov.musix;

public final class VersionBox<T> {
    private final long version;
    private final T value;

    public VersionBox(long version, T value) {
        this.version = version;
        this.value = value;
    }

    public long getVersion() {
        return version;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "VersionBox{" +
               "version=" + version +
               ", value=" + value +
               '}';
    }
}
