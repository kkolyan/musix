<%@ page import="com.nplekhanov.musix.model.User" %>
<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="com.nplekhanov.musix.AuthException" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.nplekhanov.musix.PermissionDeniedException" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String userId = (String) session.getAttribute("userId");
    if (userId == null) {
        throw new AuthException();
    }
    Musix musix = (Musix) application.getAttribute("musix");
    if (!musix.getDefaultBand().containsKey(userId)) {
        request.getRequestDispatcher("/permission_denied.jsp").forward(request, response);
        return;
    }
    User user = musix.getUser(userId);
%>
<div>
    Привет, <b title="<%=escapeHtml4(user.getUid())%>"><%=escapeHtml4(user.getFullName())%></b>
    <a class="menu" href="history.jsp">События</a>
    <a class="menu" href="opinions.jsp">Мнения</a>
    <a class="menu" href="personal_chart.jsp">Персональный чарт</a>
    <a class="menu" href="active.jsp">Активные треки</a>
    <%
        if (request.getServerName().equals("localhost")) {
            %><a href="auth.jsp">Сменить пользователя</a><%
        } else {%>  <a href="Logout">Выйти</a> <%} %>
    <%
        if (user.hasAdminRights()) {
            %><a class="menu" href="dump.jsp">Дамп</a>
            <a class="menu" href="dump_editor.jsp">Редактор</a><%
        }
    %>

</div>

