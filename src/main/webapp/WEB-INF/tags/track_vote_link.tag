<%@ tag pageEncoding="utf-8" %>
<%@ taglib prefix="mx" tagdir="/WEB-INF/tags" %>
<%@ attribute name="track" type="com.nplekhanov.musix.model.Track" required="true" %>
<%@ tag import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ tag import="java.net.URLEncoder" %>
<a href="rate.jsp?track=<%=URLEncoder.encode(track.getUuid(), "utf-8")%>"><%=escapeHtml4(track.getTrackName())%></a>