<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page import="com.nplekhanov.musix.model.User" %>
<%@ page import="com.nplekhanov.musix.JsonRepository" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.nplekhanov.musix.VersionBox" %>
<%
    String path = request.getParameter("path");
    String userId = (String) session.getAttribute("userId");
    Musix musix = (Musix) application.getAttribute("musix");
    User currentUser = musix.getUser(userId);
    if (!currentUser.isAdmin()) {
        response.sendError(503);
        return;
    }
    JsonRepository repository = (JsonRepository) application.getAttribute("repository");
    if (request.getMethod().equals("POST")) {
        long version = Long.valueOf(request.getParameter("version"));
        String content = request.getParameter("content");
        repository.setDump(path, content, version);
        if (path == null) {
            response.sendRedirect("dump.jsp");
        } else {
            response.sendRedirect("dump.jsp?path="+ URLEncoder.encode(path, "UTF-8"));
        }
        return;
    }


%>
<html>
    <body>
    <jsp:include page="header.jsp"/>
        <form method="post">
            <input type="hidden" name="path" value="<%= path == null ? "" : escapeHtml4(path)%>"/>
            <%
                VersionBox<String> dump = repository.getDump(path);
                String content = StringEscapeUtils.escapeHtml4(dump.getValue());
                int rows;
                int length = content.split("\\n").length;
                if (length > 10) {
                    rows = (int) (length * 1.5);
                } else {
                    rows = 5;
                }
                rows = Math.min(rows, 50);
            %>
            <textarea rows="<%=rows%>" cols="100" name="content"><%=content%></textarea>
            <br/>
            <input type="hidden" name="version" value="<%= dump.getVersion() %>">
            <input type="submit" value="Save"/>
        </form>
    </body>
</html>