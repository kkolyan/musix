<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="com.nplekhanov.musix.Config" %>
<%@ page import="com.nplekhanov.musix.ConfigFactory" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Config cfg = ConfigFactory.getConfig();
    String googleAppId = cfg.getGoogleClientId();
%>
<html>
<head>
    <jsp:include page="mobile.jsp"/>
    <title>Вход в систему через Google SSO</title>
    <meta name="google-signin-client_id" content="<%=escapeHtml4(googleAppId)%>">
</head>
<body>

<script src="https://apis.google.com/js/platform.js" async defer></script>

<script>
    function onSignIn(googleUser) {
        var id_token = googleUser.getAuthResponse().id_token;
        document.location.href = "GoogleAuth?token=" + encodeURIComponent(id_token)
    }
</script>
<div class="g-signin2" data-onsuccess="onSignIn"></div>
</body>
</html>
