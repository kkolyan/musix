package com.nplekhanov.musix;

import com.nplekhanov.musix.model.User;
import ua_parser.Client;
import ua_parser.Parser;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class UserAgentFilter implements Filter {
    private final Parser parser = new Parser();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(request, response);
        HttpServletRequest req = (HttpServletRequest) request;
        String userId = (String) req.getSession().getAttribute("userId");
        if (userId != null) {
            Client client = parser.parse(req.getHeader("user-agent"));
            Musix musix = (Musix) req.getServletContext().getAttribute("musix");
            musix.addUserMetric(
                userId,
                "os.family." + client.os.family,
                "userAgent.family." + client.userAgent.family,
                "device.family." + client.device.family
            );
        }
    }

    @Override
    public void destroy() {

    }
}
