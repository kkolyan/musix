<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="com.nplekhanov.musix.model.*" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.nplekhanov.musix.TrackSorting" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.LinkedHashSet" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="com.nplekhanov.musix.chart.Chart" %>
<%@ page import="java.util.TreeSet" %>
<%@ page import="static java.util.stream.Collectors.counting" %>
<%@ page import="java.util.function.Function" %>
<%@ page import="java.util.function.Predicate" %>
<%@ page import="org.apache.commons.lang3.ObjectUtils" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Мнения</title>
    <jsp:include page="mobile.jsp"/>
    <link rel="stylesheet" href="common.css" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .comment-button:hover, .comment-button-all:hover {
            cursor: pointer;
        }

        img.cell-img {
            margin-top: -7px;
            margin-right: 0;
            margin-left: 0;
            margin-bottom: -8px;
            padding: 0;
        }

        .sorting {
            padding: 2px 10px;
        }

        .selected_sorting {
            background-color: #d9d9d9;
        }
    </style>
    <script>
        var showAllComments = false;

        $(document).ready(function() {
            $(".comment-button").click(function() {
                $(this).next().toggle();
            });
            $(".comment-button-all").click(function() {
                showAllComments = ! showAllComments;
                if (showAllComments) {
                    $(".comment").show();
                } else {
                    $(".comment").hide();
                }
            });
        });
    </script>
    <script src="scroll.js" type="text/javascript"></script>
</head>
<style>
</style>
<%!
    private String href(HttpServletRequest request, String param, Object value) {
        StringBuilder s = new StringBuilder();
        Set<String> paramNames = new LinkedHashSet<String>(Collections.list(request.getParameterNames()));
        paramNames.add(param);
        for (String paramName : paramNames) {
            String val;
            if (param.equals(paramName)) {
                if (value == null) {
                    val = null;
                } else {
                    val = value.toString();
                }
            } else {
                val = request.getParameter(paramName);
            }
            if (val != null) {
                if (s.length() > 0) {
                    s.append("&");
                } else {
                    s.append("?");
                }
                s.append(paramName).append("=").append(val);
            }
        }
        return request.getRequestURI() + s;
    }

    private String sortingLabel(TrackSorting a) {
        switch (a) {
            case REDNORED_GREENCOUNT_STARS:
                return "Вето + кол-во зеленого";
            case REDNORED_STARS:
                return "Вето";
            case STARS:
                return "Простая сравнительная";
            case ALPHABET:
                return "По алфавиту";
            case ALPHABET_WOTHE:
                return "По алфавиту с учетом The";
        }
        throw new IllegalArgumentException(a+"");
    }

    private String sortingDescription(TrackSorting a) {
        String note = "" +
                "(зеленое и белое круче серого, серое круче красного, внутри цвета круче большее кол-во звезд)";
        switch (a) {
            case REDNORED_GREENCOUNT_STARS:
                return "" +
                        "1. все красное идет вниз; " +
                        "2. треки группируются по кол-ву зеленого, более зеленые группы идут вверх; " +
                        "3. внутри группы для каждого трека определяется кол-во треков, которых он круче по звездам и цвету, более звездные идут вверх;" + note;
            case REDNORED_STARS:
                return "" +
                        "1. все красное идет вниз; " +
                        "2. для каждого трека определяется кол-во треков, которых он круче по звездам и цвету, более звездные идут вверх;" + note;
            case STARS:
                return "1. для каждого трека определяется кол-во треков, которых он круче по звездам и цвету, более звездные идут вверх;" + note;
            case ALPHABET:
                return "по алфавиту игнорируя регистр";
            case ALPHABET_WOTHE:
                return "по алфавиту игнорируя регистр и The в начале";
        }
        throw new IllegalArgumentException(a+"");
    }

    private String attitudeLabel(Attitude a) {
        switch (a) {
            case UNDEFINED:
                return "Не определился";
            case DESIRED:
                return "Поддерживаю";
            case ACCEPTABLE:
                return "Согласен";
            case UNACCEPTABLE:
                return "Протестую";
        }
        throw new IllegalArgumentException(a+"");
    }
%>
<jsp:include page="header.jsp"/>
<%
    String userId = (String) session.getAttribute("userId");
    Musix musix = (Musix) application.getAttribute("musix");

    Map<String,Track> trackInfos = musix.getTrackInfo();

    int currentShowFirst;

    String showFirstAsText = request.getParameter("showFirst");
    if (showFirstAsText == null) {
        showFirstAsText = "100";
        for (Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals("showFirst")) {
                showFirstAsText = cookie.getValue();
            }
        }
    } else {
        response.addCookie(new Cookie("showFirst", showFirstAsText));
    }
    currentShowFirst = Integer.parseInt(showFirstAsText);

    Map<String, User> bandById = musix.getDefaultBand();
    Collection<User> band = bandById.values();
    final Collection<String> userIds = band.stream().map(new Function<User, String>() {
        @Override
        public String apply(final User x) {
            return x.getUid();
        }
    }).collect(Collectors.<String>toSet());
    String ignoreVotesOfUser = request.getParameter("ignoreVotesOfUser");
    final String containsText;
    if (request.getParameter("containsText") != null) {
        containsText = request.getParameter("containsText");
        session.setAttribute("containsText", containsText);
    } else {
        containsText = (String) session.getAttribute("containsText");
    }
    String nonVotedFirstBy = request.getParameter("nonVotedFirstBy");
    String sortingText = request.getParameter("sorting");
    TrackSorting sorting;
    if (sortingText != null) {
        sorting = TrackSorting.valueOf(sortingText);
    } else {
        sorting = ObjectUtils.defaultIfNull(musix.getDefaultSorting(), TrackSorting.values()[0]);
    }
    boolean hideUndefined = "true".equals(request.getParameter("hideUndefined"));
    Set<String> nonVotedFirstForUsers = nonVotedFirstBy == null
            ? Collections.<String>emptySet()
            : Collections.singleton(nonVotedFirstBy);
    Set<String> ignoreVotesOfUsers = ignoreVotesOfUser == null
            ? Collections.<String>emptySet()
            : Collections.singleton(ignoreVotesOfUser);

    String requestSource = request.getRequestURI();
    if (request.getQueryString() != null) {
        requestSource += "?" + request.getQueryString();
    }
%>
<body>
    <img class="comment-button-all" src="chat-24.png" alt="показать/скрыть все комментарии" />
    <%
    User currentUser = musix.getUser(userId);
    if (currentUser.hasAdminRights()) {
        for (TrackSorting s: TrackSorting.values()) {
            %> <span class="sorting<% if (s == sorting) {%> selected_sorting<%}%>">
                <a href="<%=href(request, "sorting", s.name())%>"
                   title="<%=escapeHtml4(sortingDescription(s)).replaceAll(";", ";\n")%>"><%=sortingLabel(s)%>
                </a> </span><%
        }
        %><p><%=escapeHtml4(sortingDescription(sorting)).replaceAll(";", ";<br/>")%></p><%
    }
    %>

<div>
    <div>

        <a href="rate.jsp">Добавить новый трек</a>
    </div>
    <%

        Collection<String> filters = new TreeSet<>();
        filters.add("");
        Function<Map.Entry<String, Long>, String> getKey = new Function<Map.Entry<String, Long>, String>() {
            @Override
            public String apply(final Map.Entry<String, Long> entry) {
                return entry.getKey();
            }
        };
        filters.addAll(musix.getTrackInfo().values().stream()
                .map(Track::getTrackName)
                .collect(Collectors.groupingBy(new Function<String, String>() {
                    @Override
                    public String apply(final String track) {
                        return track.split("[-–]")[0].trim();
                    }
                }, counting()))
                .entrySet().stream()
                .filter(new Predicate<Map.Entry<String, Long>>() {
                    @Override
                    public boolean test(final Map.Entry<String, Long> entry) {
                        return entry.getValue() > 0;
                    }
                })
                .map(getKey)
                .collect(Collectors.<String>toList()));
        if (false) for (String filter: filters) {
            %>&nbsp; <%
            String displayValue = filter.equals("") ? "Все исполнители" : filter;
            if (filter.trim().equals(containsText)) {
                %>  <%=displayValue%><%
            }
            else {
                %> <a href="<%=href(request, "containsText", filter)%>"><%= displayValue%></a> <%
            }
        }
        Collection<OpinionTrack> tracks = new Chart(musix).getTracksWithOpinions(sorting, nonVotedFirstForUsers, ignoreVotesOfUsers, new Chart.TrackFilter() {
            @Override
            public boolean matches(final Track track) {
                if (!(containsText == null || track.getTrackName().startsWith(containsText))) {
                    return false;
                }
                if (!hideUndefined) {
                    return true;
                }
                return band.stream()
                        .map(it -> track.getOpinionByUser().get(it.getUid()))
                        .map(it -> it == null ? null : it.getAttitude())
                        .map(it -> it == null ? Attitude.UNDEFINED : it)
                        .noneMatch(it -> it == Attitude.UNDEFINED);
            }
        });
    %>
</div>
<br/>
Показывать
<%
for (int showFirst: Arrays.asList(10, 20, 30, 45, 60, 75, 100, 125, 150, 200, 1000, 5000)) {
    String textValue = showFirst == Integer.MAX_VALUE ? "Все" : String.valueOf(showFirst);
    if (showFirst == currentShowFirst) {
        %><span><%=textValue%></span> <%
    } else {
        %> <a href="<%=href(request, "showFirst", showFirst)%>"><%= textValue%></a> <%
    }
}
%>
из <%=tracks.size()%>

    &nbsp;
<a href="<%=href(request, "nonVotedFirstBy", nonVotedFirstBy == null ? userId : null)%>">
    <b>мое неоцененное сверху: <%=nonVotedFirstBy == null ? "off" : "ON"%></b>
</a>
    &nbsp;
<a href="<%=href(request, "hideUndefined", hideUndefined ? null : "true")%>">
    <b>неоцененное скрыто: <%=hideUndefined ? "ON" : "off"%></b>
</a>
    <table>
        <tr>
            <th>#</th>
            <th>Трек</th>
            <th title="Позиция с учетом рейтинга">🏆</th>
            <%
                for (User u: band) {
                %> <th <%if (u.getUid().equals(ignoreVotesOfUser)) {%> style="background-color: white;" <%}%> >
                    <a href="<%=href(request, "ignoreVotesOfUser", ignoreVotesOfUser == null ? u.getUid() : null)%>">
                        <img width="64" src="<%=u.getPhotoUrl()%>" title="<%=escapeHtml4(u.getFullName())%>"/>
                    </a>
            <br/>
        </th> <%
            }
            %>
        </tr>
        <%
        int n = 0;
        for (OpinionTrack t: tracks.stream().limit(currentShowFirst).collect(Collectors.<OpinionTrack>toList())) {
            long oddity = t.getGlobalPosition() % 2;
            int desired = 0;
            int unacceptable = 0;
            for (Opinion o: t.getOpinionByUser().values()) {
                if (o.getAttitude() == Attitude.DESIRED) {
                    desired ++;
                }
                if (o.getAttitude() == Attitude.UNACCEPTABLE) {
                    unacceptable ++;
                }
            }
            %>
            <tr>
                <td class="rate<%=oddity%>"><%= ++n %></td>
                <td style="text-align: left" class="rate<%=oddity%>">
                    <tags:track_vote_link track="<%=t.getTrack()%>"/>
                    <tags:track_links trackInfo="<%=trackInfos.get(t.getTrack().getUuid())%>"/>
                    <span><%=escapeHtml4(ObjectUtils.defaultIfNull(t.getTrack().getTuningOriginal(), "-"))%> / <%=escapeHtml4(ObjectUtils.defaultIfNull(t.getTrack().getTuningTarget(), "-"))%></span>
                </td>
                <% if (true) {
                    Collection<String> ratedFullNames = new ArrayList<>();
                    for (User user: t.getRatedWithinGroup().keySet()) {
                        long rate = t.getRatedWithinGroup().get(user);
                        ratedFullNames.add(user.getFullName()+": "+ rate);
                    }
                    Collection<String> resultsWithinGroup = new ArrayList<>();

                    resultsWithinGroup.add("total: "+t.getScoreWithinGroup());

                    resultsWithinGroup.add("\tSuperiors:");
                    resultsWithinGroup.addAll(t.getSuperiorWithinGroup());

                    resultsWithinGroup.add("\tEquals:");
                    resultsWithinGroup.addAll(t.getEqualWithinGroup());

                    resultsWithinGroup.add("\tInferiors:");
                    resultsWithinGroup.addAll(t.getInferiorWithinGroup());

                    Collection<String> resultsWithinGroupNames = resultsWithinGroup.stream()
                    .map(it -> {
                        Track track = trackInfos.get(it);
                        if (track == null) {
                            return it;
                        }
                        return track.getTrackName();
                    })
                    .collect(Collectors.toList());

                    %><!-- <td class="rate<%=desired%>" title="Рейтинги:<%="\n"+String.join("\n", ratedFullNames)%>" onclick="alert(this.title)"><%=desired%> / <%=t.getGlobalPosition()%> </td> --><%
                    %><td class="rate<%=oddity%>" title="<%=escapeHtml4(String.join("\n", resultsWithinGroupNames))%>"><%
                Predicate<Map.Entry<String, Opinion>> isMember = new Predicate<Map.Entry<String, Opinion>>() {
                    @Override
                    public boolean test(final Map.Entry<String, Opinion> x) {
                        return userIds.contains(x.getKey());
                    }
                };
                Predicate<Opinion> isUnacceptable = new Predicate<Opinion>() {
                    @Override
                    public boolean test(final Opinion x) {
                        return x.getAttitude() == Attitude.UNACCEPTABLE;
                    }
                };
                Function<Map.Entry<String, Opinion>, Opinion> getValue = new Function<Map.Entry<String, Opinion>, Opinion>() {
                    @Override
                    public Opinion apply(final Map.Entry<String, Opinion> entry) {
                        return entry.getValue();
                    }
                };
                if (t.getOpinionByUser().entrySet().stream().filter(isMember).map(getValue).noneMatch(isUnacceptable)){%><%=t.getGlobalPosition()%><%}%> </td> <%
                } else {
                    %><td></td> <%
                }%>
                <%
                    for (User user: band) {
                        Opinion o = t.getOpinionByUser().get(user.getUid());
                        if (o != null) {
                            %>
                                <td class="<%=o.getAttitude()%> opinions" style="max-width: 200px" title="<%=attitudeLabel(o.getAttitude())%>">
                                    <% if (user.getUid().equals(userId)) {%>
                                        <form style="display: inline" action="PersonalTrackRating" class="mini-form" method="post" onsubmit="return updateScrollState(this)">
                                            <input type="hidden" name="track" value="<%=escapeHtml4(t.getTrack().getUuid())%>">
                                            <input type="hidden" name="step" value="-1">
                                            <input type="hidden" name="source" value="<%=requestSource%>">
                                            <input type="submit" value="-" class="rating-button left-round"/>
                                            <input type="hidden" name="scrollState" value="scrollState1"/>
                                        </form>
                                        <form style="display: inline" action="PersonalTrackRating" class="mini-form" method="post" onsubmit="return updateScrollState(this)">
                                            <input type="hidden" name="track" value="<%=escapeHtml4(t.getTrack().getUuid())%>">
                                            <input type="hidden" name="step" value="1">
                                            <input type="hidden" name="source" value="<%=requestSource%>">
                                            <input type="submit" value="+" class="rating-button right-round"/>
                                            <input type="hidden" name="scrollState" value="scrollState2"/>
                                        </form>
                                    <%}%>
                                    <span class="stars"><%
                                        for (int i = 0; i < o.getRating(); i ++) {
                                    %>*<%
                                        }
                                    %></span>
                                    <% if (o.isNotEnoughSkills()) {
                                    %> <img src="Error-25.png" class="cell-img" alt="(Too hard)" title="Недостаточно навыка" /> <%
                                    }%>
                                    <% if (o.isRejectToPerform()) {
                                    %> <img src="icons8-mute-unmute-24.png" class="cell-img" alt="(Donwanna perform)" title="Не хочу исполнять, но готов отдохнуть и послушать" /> <%
                                    }%>
                                    <%
                                        if (o.getComment() != null && !o.getComment().trim().isEmpty()) {
                                    %><img class="comment-button cell-img" src="chat-24.png" alt="comment" title="<%=escapeHtml4(o.getComment())%>"/><span style="display: none;" class="comment"><%=escapeHtml4(o.getComment())%></span>  <%
                                    }
                                %>
                                </td>
                            <%
                        } else {
                            %>
                                <td title="Не проголосовал">
                                </td>
                            <%
                        }
                    }
                %>
            </tr>
            <%
        }
        %>
    </table>

<table>
    <%
    for (Attitude a: Attitude.values()) {
        %> <tr>
            <td class="<%=a%>"></td>
            <td><%=attitudeLabel(a)%></td>
        </tr> <%
    }
    %>

</table>
</body>
</html>
