<%@ page import="com.nplekhanov.musix.Musix" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String track = request.getParameter("track");
    Musix musix = (Musix) application.getAttribute("musix");
    musix.activateTrack(track);
    response.sendRedirect("active.jsp");
%>