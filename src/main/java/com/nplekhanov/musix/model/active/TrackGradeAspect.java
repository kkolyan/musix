package com.nplekhanov.musix.model.active;

/**
 * Created by nplekhanov on 26/12/2017.
 */
public enum TrackGradeAspect {
    FUN_OF_PLAYING,
    READY_TO_STAGE_PROGRESS,
    READY_TO_WORK_PROGRESS,
    DESIRED_TEMPO_PROGRESS(0.5),
    ORIGINAL_BOTTLENECKS_PROGRESS;

    private final double bottom;

    TrackGradeAspect() {
        this(0);
    }

    TrackGradeAspect(double bottom) {
        this.bottom = bottom;
    }

    public static int applyGradeBottom(double value, TrackGradeAspect aspect) {
        if (value < aspect.getBottom()) {
            return 0;
        }
        double v = (value - aspect.getBottom()) / (1 - aspect.getBottom());
        return ((int) Math.round(v * 10)) * 10;
    }

    public double getBottom() {
        return bottom;
    }
}
