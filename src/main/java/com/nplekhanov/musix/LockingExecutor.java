package com.nplekhanov.musix;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LockingExecutor {
    private static final Logger logger = LoggerFactory.getLogger(LockingExecutor.class);

    private final ReadWriteLock lock = new ReentrantReadWriteLock(true);

    private final ContextInfoProvider contextInfoProvider;
    private final RandomAccessFile lockFile;

    public LockingExecutor(ContextInfoProvider contextInfoProvider, File lockFilePath) {
        this.contextInfoProvider = contextInfoProvider;
        try {
            lockFilePath.getParentFile().mkdirs();
            lockFile = new RandomAccessFile(lockFilePath, "rw");
            FileLock fileLock = lockFile.getChannel().tryLock();
            if (fileLock == null || !fileLock.isValid()) {
                throw new IllegalStateException("Musix cannot be run multiple instances in parallel");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public interface SynchronizedTask<T> {
        T doSynchronized() throws IOException;
    }

    public interface ContextInfoProvider {
        Object getContext();
    }

    public <T> T withReadLock(SynchronizedTask<T> task) throws IOException {
        Lock readLock = lock.readLock();
        logger.info("locking for read. context: {}", contextInfoProvider.getContext());
        readLock.lock();
        try {
            return task.doSynchronized();
        } finally {
            readLock.unlock();
            logger.info("unlocked read");
        }
    }

    public <T> T withWriteLock(SynchronizedTask<T> task) throws IOException {
        Lock readLock = lock.writeLock();
        logger.info("locking for write. context: {}", contextInfoProvider.getContext());
        readLock.lock();
        try {
            return task.doSynchronized();
        } finally {
            readLock.unlock();
            logger.info("unlocked write");
        }
    }
}
