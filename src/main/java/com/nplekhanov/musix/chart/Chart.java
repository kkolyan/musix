package com.nplekhanov.musix.chart;

import com.nplekhanov.musix.Musix;
import com.nplekhanov.musix.TrackSorting;
import com.nplekhanov.musix.model.Attitude;
import com.nplekhanov.musix.model.Opinion;
import com.nplekhanov.musix.model.OpinionTrack;
import com.nplekhanov.musix.model.Track;
import com.nplekhanov.musix.model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Chart {
    private final Musix musix;

    public Chart(final Musix musix) {
        this.musix = musix;
    }

    public Collection<OpinionTrack> getTracksWithOpinions(
        TrackSorting trackSorting,
        Collection<String> nonVotedFirstForUsers,
        Collection<String> ignoreVotesOfUsers, final TrackFilter trackFilter
    ) {

        Collection<User> users = musix.getDefaultBand().values();
        Collection<OpinionTrack> tracks = aggregateTracksWithOpinions(users, trackFilter);
        return sortTracksWithOpinions(tracks, users.stream().filter(user -> !ignoreVotesOfUsers.contains(user.getUid())).collect(Collectors.toList()), trackSorting, nonVotedFirstForUsers);
    }

    private Collection<OpinionTrack> sortTracksWithOpinions(
        Collection<OpinionTrack> tracks,
        Collection<User> users,
        TrackSorting trackSorting,
        Collection<String> nonVotedFirstForUsers
    ) {

        for (OpinionTrack track : tracks) {
            track.setRatedWithinGroup(new HashMap<>());
            track.setGroupResults(new HashMap<>());
        }
        if (trackSorting == TrackSorting.ALPHABET) {
            return tracks.stream()
                .sorted(Comparator.comparing(x -> x.getTrack().getTrackName().toLowerCase()))
                .collect(Collectors.toList());
        }
        if (trackSorting == TrackSorting.ALPHABET_WOTHE) {
            return tracks.stream()
                .sorted(Comparator.comparing(x -> {
                    String lowerCase = x.getTrack().getTrackName().toLowerCase();
                    if (lowerCase.startsWith("the")) {
                        return lowerCase.substring("the".length()).trim();
                    }
                    return lowerCase;
                }))
                .collect(Collectors.toList());
        }
        if (trackSorting == TrackSorting.STARS) {
            return sortGroup(tracks, users).getTracks();
        }
        if (trackSorting == TrackSorting.REDNORED_GREENCOUNT_STARS) {

            Map<Long, List<OpinionTrack>> groups = tracks.stream()
                .collect(Collectors.groupingBy(x -> {
                    long order = 0;
                    int unacceptableCount = 0;
                    for (User user : users) {
                        Opinion opinion = x.getOpinionByUser().get(user.getUid());
                        if ((opinion == null || opinion.getAttitude() == Attitude.UNDEFINED) && nonVotedFirstForUsers.contains(user.getUid())) {
                            return -90000L;
                        }

                        if (opinion == null || opinion.getAttitude() == Attitude.UNDEFINED) {
                            order += -100;
                        } else if (opinion.getAttitude() == Attitude.DESIRED) {
                            order += -100;
                        } else {
                            if (opinion.getAttitude() == Attitude.UNACCEPTABLE) {
                                order += 100;
                                unacceptableCount++;
                            }
                        }
                    }
                    if (unacceptableCount > 0) {
                        order += 90000;
                    }

                    return order;
                }));

            return postProcessGroups(users, groups);
        }
        if (trackSorting == TrackSorting.REDNORED_STARS) {
            Map<Long, List<OpinionTrack>> groups = tracks.stream()
                .collect(Collectors.groupingBy(x -> {
                    long order = 0;
                    int unacceptableCount = 0;
                    for (User user : users) {
                        Opinion opinion = x.getOpinionByUser().get(user.getUid());
                        if (opinion != null && opinion.getAttitude() == Attitude.UNACCEPTABLE) {
                            unacceptableCount++;
                        }
                    }
                    if (unacceptableCount > 0) {
                        order += 90000;
                    }

                    return order;
                }));
            return postProcessGroups(users, groups);
        }
        throw new IllegalArgumentException("trackSorting: " + trackSorting);
    }

    private List<OpinionTrack> postProcessGroups(Collection<User> users, Map<Long, List<OpinionTrack>> groups) {
        TreeMap<Long, List<OpinionTrack>> sortedGroups = new TreeMap<>(groups);

        int offset = 0;
        List<OpinionTrack> list = new ArrayList<>();
        for (List<OpinionTrack> group : sortedGroups.values()) {
            Group sortedGroup = sortGroup(group, users);
            for (OpinionTrack track : sortedGroup.getTracks()) {
                track.setGlobalPosition(track.getPositionInsideGroup() + offset);
                list.add(track);
            }
            offset += sortedGroup.subGroupsCount;
        }
        return list;
    }

    private static class Group {
        private int subGroupsCount;
        private List<OpinionTrack> tracks;

        public Group(int subGroupsCount, List<OpinionTrack> tracks) {
            this.subGroupsCount = subGroupsCount;
            this.tracks = tracks;
        }

        public int getSubGroupsCount() {
            return subGroupsCount;
        }

        public List<OpinionTrack> getTracks() {
            return tracks;
        }
    }

    private Group sortGroup(Collection<OpinionTrack> tracks, Collection<User> users) {
        for (OpinionTrack ot : tracks) {
            ot.setRatedWithinGroup(new HashMap<>());
            for (User user : users) {
                ot.getRatedWithinGroup().put(user, calculateUserRating(ot, user.getUid()));
            }
        }
        Comparator<OpinionTrack> cmp = (o1, o2) -> {
            LongSummaryStatistics stat = new LongSummaryStatistics();
            for (User user : users) {
                long c1 = calculateUserRating(o1, user.getUid());
                long c2 = calculateUserRating(o2, user.getUid());
                long diff = c2 - c1;
                if (diff != 0) {
                    diff = diff / Math.abs(diff);
                }
                stat.accept(diff);
            }
            if (stat.getAverage() > 0) {
                return 1;
            }
            if (stat.getAverage() < 0) {
                return -1;
            }
            return 0;
        };
        for (OpinionTrack track : tracks) {
            Map<String, Integer> groupResults = new LinkedHashMap<>();
            for (OpinionTrack opponent : tracks) {
                groupResults.put(opponent.getTrack().getUuid(), -cmp.compare(track, opponent));
            }
            track.setGroupResults(groupResults);
        }
        Map<OpinionTrack, Collection<OpinionTrack>> subGroups = new TreeMap<>(Comparator.comparing(x -> -x.getScoreWithinGroup()));
        for (OpinionTrack track : tracks) {
            subGroups.compute(track, (k, v) -> v != null ? v : new ArrayList<>())
                .add(track);
        }
        int i = 0;
        for (Collection<OpinionTrack> subGroup : subGroups.values()) {
            i++;
            for (OpinionTrack ot : subGroup) {
                ot.setPositionInsideGroup(i);
            }
        }
        return new Group(subGroups.size(), subGroups.values().stream().flatMap(Collection::stream).collect(Collectors.toList()));
    }

    private long calculateUserRating(OpinionTrack track, String userId) {
        Opinion opinion = track.getOpinionByUser().get(userId);
        if (opinion == null) {
            return Opinion.MAX_RATING;
        }
        long attitudeRatingComponent = getAttitudeRatingComponent(opinion);
        return opinion.getRating() + attitudeRatingComponent;
    }

    private long getAttitudeRatingComponent(Opinion opinion) {
        if (opinion.getAttitude() == null) {
            return 0;
        }
        switch (opinion.getAttitude()) {
            case UNDEFINED:
            case DESIRED:
                return 0;
            case ACCEPTABLE:
                return -1000;
            case UNACCEPTABLE:
                return -1000000;
            default:
                throw new IllegalArgumentException("opinion: " + opinion);
        }
    }

    public interface TrackFilter {
        boolean matches(Track track);
    }

    private Collection<OpinionTrack> aggregateTracksWithOpinions(Collection<User> users, final TrackFilter filter) {

        Set<String> activeTracks = musix.getActiveTracks().keySet();

        Collection<OpinionTrack> tracks = new ArrayList<>();

        for (final Track track : musix.getTrackInfo().values()) {
            if (!filter.matches(track)) {
                continue;
            }
            if (activeTracks.contains(track.getUuid())) {
                continue;
            }
            if (users.stream().noneMatch(user -> track.getOpinionByUser().containsKey(user.getUid()))) {
                continue;
            }
            OpinionTrack t = new OpinionTrack();
            t.setTrack(track);
            t.setOpinionByUser(track.getOpinionByUser());
            tracks.add(t);
        }
        return tracks;
    }
}
