package com.nplekhanov.musix.migrations;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nplekhanov.musix.JsonRepository;
import com.nplekhanov.musix.VersionBox;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

public class MoveOpinionsFromUserToTracks {
    public void apply(final JsonRepository repository) throws IOException {
        synchronized (repository) {
            VersionBox<String> dump1 = repository.getDump(null);
            String dump = dump1.getValue();
            JsonNode db = repository.createObjectMapper().readValue(dump, JsonNode.class);
            JsonNode users = db.get("users");
            if (users.size() == 0) {
                return;
            }
            JsonNode firstUser = users.get(0);
            JsonNode firstOpinionByTrack = firstUser.get("opinionByTrack");
            if (firstOpinionByTrack == null) {
                return;
            }
            Set<String> tracks = new LinkedHashSet<>();
            for (final JsonNode user : users) {
                ObjectNode node = (ObjectNode) user;
                JsonNode opinionByTrack = node.get("opinionByTrack");
                opinionByTrack.fieldNames().forEachRemaining(tracks::add);
            }
            for (final JsonNode track : db.get("tracks")) {
                tracks.remove(track.get("trackName").asText());
                ObjectNode trackInfo = (ObjectNode) track;
                calculateOpinions(db, trackInfo);
            }
            ArrayNode tracksRoot = (ArrayNode) db.get("tracks");
            for (final String trackName : tracks) {
                ObjectNode trackInfo = tracksRoot.addObject();
                trackInfo.put("trackName", trackName);
                calculateOpinions(db, trackInfo);
            }
            for (final JsonNode user : users) {
                ObjectNode node = (ObjectNode) user;
                node.remove("opinionByTrack");
            }
            repository.setDump(null, repository.createObjectMapper()
                    .configure(SerializationFeature.INDENT_OUTPUT, true)
                    .writeValueAsString(db), dump1.getVersion());
        }
    }

    private void calculateOpinions(final JsonNode db, final ObjectNode trackInfo) {
        ObjectNode field = trackInfo.putObject("opinionByUser");
        for (final JsonNode user : db.get("users")) {
            JsonNode opinion = user.get("opinionByTrack").get(trackInfo.get("trackName").asText());
            if (opinion != null) {
                field.set(user.get("uid").asText(), opinion);
            }
        }
    }
}
