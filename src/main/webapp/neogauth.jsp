<%@ page import="static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4" %>
<%@ page import="com.nplekhanov.musix.Config" %>
<%@ page import="com.nplekhanov.musix.ConfigFactory" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Config cfg = ConfigFactory.getConfig();
    String googleAppId = cfg.getGoogleClientId();
%>
<html>
<head>
    <jsp:include page="mobile.jsp"/>
    <title>Вход в систему через Google SSO</title>
</head>
<body>
<script src="https://accounts.google.com/gsi/client" async defer></script>
<div id="g_id_onload"
     data-client_id="<%=escapeHtml4(googleAppId)%>"
     data-ux_mode="redirect"
     data-login_uri="https://musix.nplekhanov.com/jc/GoogleAuth">
</div>
<div class="g_id_signin" data-type="standard"></div>
</body>
</html>
