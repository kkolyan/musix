package com.nplekhanov.musix.model;

import com.nplekhanov.musix.model.active.ActiveTrackOpinion;

import java.util.Map;

/**
 * Created by nplekhanov on 6/24/2017.
 */
public class ActiveTrack {
    private String uuid;
    private Map<String, ActiveTrackOpinion> opinionByUser;
    private int priority;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Map<String, ActiveTrackOpinion> getOpinionByUser() {
        return opinionByUser;
    }

    public void setOpinionByUser(Map<String, ActiveTrackOpinion> opinionByUser) {
        this.opinionByUser = opinionByUser;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
