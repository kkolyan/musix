package com.nplekhanov.musix.model;

/**
 * Created by nplekhanov on 18/01/2018.
 */
public class TrackLinkAddedEvent extends UserTrackEvent {
    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
