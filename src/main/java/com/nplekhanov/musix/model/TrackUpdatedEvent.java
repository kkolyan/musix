package com.nplekhanov.musix.model;

/**
 * Created by nplekhanov on 18/01/2018.
 */
public class TrackUpdatedEvent extends UserTrackEvent {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
