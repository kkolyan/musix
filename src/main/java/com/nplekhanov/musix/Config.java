package com.nplekhanov.musix;

public interface Config {

    String getVkAppId();

    String getGoogleClientId();

    String getSecretKey();

    String getDevIp();

    String getBackupSuffixDateTimeFormat();

    String getTracksExternalListeningPage();

    String getGithubPat();

    String getRepositoryFilePath();
}
